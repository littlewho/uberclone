/**
 * View Models used by Spring MVC REST controllers.
 */
package com.uberclone.location.web.rest.vm;
