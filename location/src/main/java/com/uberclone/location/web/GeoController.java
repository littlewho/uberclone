package com.uberclone.location.web;

import com.uberclone.location.service.GeoService;
import com.uberclone.location.service.dto.LocationDTO;
import com.uberclone.location.service.dto.NearbySubscribersDTO;
import com.uberclone.location.service.dto.SubscribeDTO;
import com.uberclone.location.service.dto.UpdateDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping("/api")
public class GeoController {
    private final GeoService geoService;

    public GeoController(GeoService geoService) {
        this.geoService = geoService;
    }

    @PostMapping("/subscribe")
    public ResponseEntity<Void> subscribe(
        Principal principal,
        @Valid @RequestBody SubscribeDTO subscribeDTO
    ) {
        geoService.subscribe(principal.getName(), subscribeDTO);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/unsubscribe")
    public ResponseEntity<Void> unsubscribe(Principal principal) {
        geoService.unsubscribe(principal.getName());
        return ResponseEntity.ok().build();
    }

    @PostMapping("/update")
    public ResponseEntity<Void> update(Principal principal, @Valid @RequestBody UpdateDTO updateDTO) {
        geoService.update(principal.getName(), updateDTO);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/nearby")
    public ResponseEntity<NearbySubscribersDTO> nearby(@Valid LocationDTO locationDTO) {
        var result = geoService.getNearbySubscribers(locationDTO);
        return ResponseEntity.ok(result);
    }
}
