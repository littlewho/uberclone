package com.uberclone.location.domain;

import java.util.Comparator;

public class DistanceComparator implements Comparator<Distance> {
    @Override
    public int compare(Distance o1, Distance o2) {
        return Integer.compare(
          o1.getValue().inMeters(),
          o2.getValue().inMeters()
        );
    }
}
