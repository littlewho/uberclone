package com.uberclone.location.domain;

import lombok.Data;

@Data
public class Distance {
    private final DistanceUnit value;

    public Distance(Location location1, Location location2) {
        var distInMeters = calculate(
            location1.getLatitude(),
            location2.getLatitude(),
            location1.getLongitude(),
            location2.getLongitude()
        );
        this.value = DistanceUnit.fromMeters(distInMeters);
    }

    private final static double AVERAGE_RADIUS_OF_EARTH = 6371;
    private static double calculate(double lat1, double lat2, double lon1,
                                    double lon2) {
        double latDistance = Math.toRadians(lat1 - lat2);
        double lngDistance = Math.toRadians(lon1 - lon2);

        double a = (Math.sin(latDistance / 2) * Math.sin(latDistance / 2)) +
            (Math.cos(Math.toRadians(lat1))) *
                (Math.cos(Math.toRadians(lat2))) *
                (Math.sin(lngDistance / 2)) *
                (Math.sin(lngDistance / 2));

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return AVERAGE_RADIUS_OF_EARTH * c * 1000;
    }
}
