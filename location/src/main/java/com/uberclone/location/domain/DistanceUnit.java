package com.uberclone.location.domain;

import lombok.Data;

@Data
public class DistanceUnit {
    private final int units;

    public DistanceUnit(int meters) {
        this.units = meters;
    }

    public DistanceUnit(int meters, int kilometers) {
        this.units = meters + kilometers*1000;
    }

    public int inMeters() {
        return units;
    }

    public int inKilometers() {
        return units / 1000;
    }

    public static DistanceUnit fromMeters(int meters) {
        return new DistanceUnit(meters);
    }

    public static DistanceUnit fromMeters(double meters) {
        return new DistanceUnit((int) Math.round(meters));
    }


    public static DistanceUnit fromKilometers(int kilometers) {
        return new DistanceUnit(kilometers * 1000);
    }
}
