package com.uberclone.location.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@Data
@AllArgsConstructor
public class Location {
    private final double longitude;
    private final double latitude;
}
