package com.uberclone.location.domain;

import lombok.Data;

@Data
public class GeoMapNode {
    private final String key;
    private Location location;
    private final GeoMapNodeData data;

    public GeoMapNode(String key, Location location, GeoMapNodeData data) {
        this.key = key;
        this.location = location;
        this.data = data;
    }
}
