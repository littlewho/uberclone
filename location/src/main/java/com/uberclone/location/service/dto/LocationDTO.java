package com.uberclone.location.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocationDTO implements Serializable {
    @NotNull
    private double longitude;
    @NotNull
    private double latitude;
}
