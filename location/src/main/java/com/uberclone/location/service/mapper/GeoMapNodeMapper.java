package com.uberclone.location.service.mapper;

import com.uberclone.location.domain.GeoMapNode;
import com.uberclone.location.service.dto.GeoMapNodeDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {
    LocationMapper.class,
    GeoMapNodeDataMapper.class
})
public interface GeoMapNodeMapper extends EntityMapper<GeoMapNodeDTO, GeoMapNode> {
}
