package com.uberclone.location.service;

import com.uberclone.location.domain.DistanceUnit;
import com.uberclone.location.domain.GeoMapNode;
import com.uberclone.location.domain.GeoMapNodeData;
import com.uberclone.location.repository.GeoMapRepository;
import com.uberclone.location.service.dto.LocationDTO;
import com.uberclone.location.service.dto.NearbySubscribersDTO;
import com.uberclone.location.service.dto.SubscribeDTO;
import com.uberclone.location.service.dto.UpdateDTO;
import com.uberclone.location.service.mapper.GeoMapNodeMapper;
import com.uberclone.location.service.mapper.LocationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class GeoService {
    private final Logger log = LoggerFactory.getLogger(GeoService.class);
    private final GeoMapRepository geoMapRepository;
    private final LocationMapper locationMapper;
    private final GeoMapNodeMapper geoMapNodeMapper;

    public GeoService(
        GeoMapRepository geoMapRepository,
        LocationMapper locationMapper,
        GeoMapNodeMapper geoMapNodeMapper) {
        this.geoMapRepository = geoMapRepository;
        this.locationMapper = locationMapper;
        this.geoMapNodeMapper = geoMapNodeMapper;
    }

    public void subscribe(String key, SubscribeDTO subscribeDTO) {
        log.debug("New driver subscribed");
        geoMapRepository.addPoint(new GeoMapNode(
            key,
            locationMapper.toEntity(subscribeDTO.getLocation()),
            new GeoMapNodeData()
        ));
    }

    public void unsubscribe(String key) {
        log.debug("Some driver unsubscribed");
        geoMapRepository.deletePoint(key);
    }

    public void update(String key, UpdateDTO updateDTO) {
        log.debug("Some driver updated their location");
        geoMapRepository.updatePoint(key, locationMapper.toEntity(updateDTO.getLocation()));
    }

    private final DistanceUnit defaultDistance = DistanceUnit.fromKilometers(10);
    public NearbySubscribersDTO getNearbySubscribers(LocationDTO locationDTO) {
        var nodes = geoMapRepository.getPointsInRange(locationMapper.toEntity(locationDTO), defaultDistance);
        return new NearbySubscribersDTO(
            geoMapNodeMapper.toDto(nodes)
        );
    }
}
