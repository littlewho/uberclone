package com.uberclone.location.service.mapper;

import com.uberclone.location.domain.Location;
import com.uberclone.location.service.dto.LocationDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface LocationMapper extends EntityMapper<LocationDTO, Location> {
}
