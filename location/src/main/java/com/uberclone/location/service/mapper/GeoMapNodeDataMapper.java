package com.uberclone.location.service.mapper;

import com.uberclone.location.domain.GeoMapNodeData;
import com.uberclone.location.service.dto.GeoMapNodeDataDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface GeoMapNodeDataMapper extends EntityMapper<GeoMapNodeDataDTO, GeoMapNodeData> {
}
