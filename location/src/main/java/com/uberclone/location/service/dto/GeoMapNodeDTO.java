package com.uberclone.location.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GeoMapNodeDTO implements Serializable {
    private String key;
    private LocationDTO location;
    private GeoMapNodeDataDTO data;
}
