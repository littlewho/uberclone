package com.uberclone.location.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NearbySubscribersDTO implements Serializable {
    List<GeoMapNodeDTO> subscribers;
}
