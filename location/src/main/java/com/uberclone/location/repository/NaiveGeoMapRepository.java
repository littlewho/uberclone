package com.uberclone.location.repository;

import com.uberclone.location.domain.*;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This is a naive implementation of an in-memory storage.
 * All entities are stored inside lists and sets.
 * Every method is (entirely) synchronized so only
 * one thread can alter it at a time. It is highly inefficient,
 * but it works for our demo purpose.
 */

@Repository
public class NaiveGeoMapRepository implements GeoMapRepository {
    private final List<GeoMapNode> points = Collections.synchronizedList(new LinkedList<>());
    private final Set<String> keys = Collections.synchronizedSet(new HashSet<>());

    @Override
    public synchronized List<GeoMapNode> getPointsAround(Location location, int count) {
        return points.stream().sorted(
            (p1, p2) -> new DistanceComparator().compare(
                new Distance(p1.getLocation(), location),
                new Distance(p2.getLocation(), location)
            )
        ).limit(count).collect(Collectors.toList());
    }

    @Override
    public synchronized List<GeoMapNode> getPointsInRange(Location location, DistanceUnit units) {
        return points.stream().filter(point ->
            new Distance(location, point.getLocation()).getValue().inMeters() <= units.inMeters()
        ).collect(Collectors.toList());
    }

    @Override
    public synchronized void addPoint(GeoMapNode point) {
        if (keys.contains(point.getKey())) return ;
        keys.add(point.getKey());
        points.add(point);
    }

    @Override
    public synchronized void updatePoint(String key, Location location) throws KeyNotFoundException {
        if (!keys.contains(key)) throw new KeyNotFoundException("update point");
        points.forEach(point -> {
            if (point.getKey().equals(key)) {
                point.setLocation(location);
            }
        });
    }

    @Override
    public synchronized void deletePoint(String key) throws KeyNotFoundException {
        if (!keys.contains(key)) throw new KeyNotFoundException("delete point");
        points.removeIf(point -> point.getKey().equals(key));
        keys.remove(key);
    }

    @Override
    public synchronized Optional<GeoMapNode> getPointByKey(String key) {
        return points
            .stream()
            .filter(point -> point.getKey().equals(key))
            .findFirst();
    }
}
