package com.uberclone.location.repository;

import com.uberclone.location.domain.DistanceUnit;
import com.uberclone.location.domain.GeoMapNode;
import com.uberclone.location.domain.Location;

import java.util.List;
import java.util.Optional;

public interface GeoMapRepository {
    List<GeoMapNode> getPointsAround(Location location, int count);
    List<GeoMapNode> getPointsInRange(Location location, DistanceUnit units);
    void addPoint(GeoMapNode point);
    void updatePoint(String key, Location location) throws KeyNotFoundException;
    void deletePoint(String key) throws KeyNotFoundException;
    Optional<GeoMapNode> getPointByKey(String key);
}
