package com.uberclone.location.domain;


import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class DistanceUnitTest {

    @Test
    public void calculateDistance() {
        var loc1 = new Location(25, 24);
        var loc2 = new Location(26, 23);
        var dist = new Distance(loc1, loc2);

        assertThat(dist.getValue().inKilometers()).isEqualTo(150);

        var loc3 = new Location(46.076636, 24.623333);
        var loc4 = new Location(46.076636, 24.823333);
        var dist2 = new Distance(loc3, loc4);

        assertThat(dist2.getValue().inKilometers()).isLessThanOrEqualTo(50);
    }
}
