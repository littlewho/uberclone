package com.uberclone.location.repository;

import com.uberclone.location.domain.*;
import javassist.NotFoundException;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

public class NaiveGeoMapRepositoryUnitTest {
    @Test
    void addAndGetPoints() {
        var repo = new NaiveGeoMapRepository();

        for (int i = 0; i < 5; i++) {
            var key = "test" + i;
            repo.addPoint(new GeoMapNode(
                key,
                new Location(i, i),
                new GeoMapNodeData()
            ));

            var returned = repo.getPointByKey(key);
            assertThat(returned).isPresent();
            var valid = returned.get();
            assertThat(valid.getLocation()).isEqualTo(new Location(i, i));
        }
    }

    @Test
    void getPointsAround() {
        var center = new Location(46.076636, 24.623333);
        var repo = new NaiveGeoMapRepository();

        // Under 50km
        repo.addPoint(
            new GeoMapNode(
                "test1",
                new Location(46.076636, 24.823333),
                new GeoMapNodeData()
            )
        );
        repo.addPoint(
            new GeoMapNode(
                "test2",
                new Location(46.176636, 24.823333),
                new GeoMapNodeData()
            )
        );
        repo.addPoint(
            new GeoMapNode(
                "test3",
                new Location(46.196636, 24.823333),
                new GeoMapNodeData()
            )
        );

        // Above 50km
        repo.addPoint(
            new GeoMapNode(
                "test4",
                new Location(47.176636, 24.823333),
                new GeoMapNodeData()
            )
        );
        repo.addPoint(
            new GeoMapNode(
                "test5",
                new Location(46.196636, 25.823333),
                new GeoMapNodeData()
            )
        );

        var found = repo.getPointsInRange(center, DistanceUnit.fromKilometers(50));
        assertThat(found.size()).isEqualTo(3);
    }

    @Test
    void getNearestPoints() {
        var center = new Location(46.076636, 24.623333);
        var repo = new NaiveGeoMapRepository();

        repo.addPoint(
            new GeoMapNode(
                "test1",
                new Location(46.076636, 24.823333),
                new GeoMapNodeData()
            )
        );
        repo.addPoint(
            new GeoMapNode(
                "test2",
                new Location(46.076636, 24.903333),
                new GeoMapNodeData()
            )
        );

        var found = repo.getPointsAround(center, 1);
        assertThat(found.get(0).getKey()).isEqualTo("test1");
    }

    @Test
    void updatePoints() throws NotFoundException {
        var repo = new NaiveGeoMapRepository();
        repo.addPoint(new GeoMapNode(
            "test1",
            new Location(20, 30),
            new GeoMapNodeData()
        ));

        repo.updatePoint("test1", new Location(99, 99));
        var found = repo.getPointByKey("test1");
        assertThat(found).isPresent();
        assertThat(found.get().getLocation()).isEqualTo(new Location(99, 99));
    }

    @Test
    void deletePoints() {
        var repo = new NaiveGeoMapRepository();
        repo.addPoint(new GeoMapNode(
            "test1",
            new Location(20, 30),
            new GeoMapNodeData()
        ));
        repo.addPoint(new GeoMapNode(
            "test2",
            new Location(25, 35),
            new GeoMapNodeData()
        ));

        try {
            repo.deletePoint("test1");
        } catch (KeyNotFoundException ignored) {
            fail("key1 not found");
        }

        var notFound = repo.getPointByKey("test1");
        assertThat(notFound).isEmpty();
        var found = repo.getPointByKey("test2");
        assertThat(found).isPresent();
    }
}
