package com.uberclone.location.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.uberclone.location.IntegrationTest;
import com.uberclone.location.domain.Location;
import com.uberclone.location.repository.GeoMapRepository;
import com.uberclone.location.service.dto.*;
import com.uberclone.location.service.mapper.LocationMapper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser(username = GeoControllerIT.username)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GeoControllerIT {
    public static final String username = "test";

    private static final String SUBSCRIBE_URL = "/api/subscribe";
    private static final String UNSUBSCRIBE_URL = "/api/unsubscribe";
    private static final String NEARBY_URL = "/api/nearby";
    private static final Location baseLocation = new Location(25, 25);

    @Autowired
    private GeoMapRepository geoMapRepository;

    @Autowired
    private LocationMapper locationMapper;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Order(0)
    void subscribe() throws Exception {
        var dto = new SubscribeDTO(
            locationMapper.toDto(baseLocation)
        );

        mockMvc.perform(
            MockMvcRequestBuilders.post(SUBSCRIBE_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dto))
        ).andExpect(MockMvcResultMatchers.status().isOk());

        assertThat(geoMapRepository.getPointsAround(baseLocation, 1).size()).isEqualTo(1);
    }

    @Test
    @Order(1)
    void nearby() throws Exception {
        var result = mockMvc.perform(
            MockMvcRequestBuilders.get(NEARBY_URL + "?longitude=25.00&latitude=25.00")
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isOk())
        .andReturn();

        var list = new ArrayList<GeoMapNodeDTO>();
        list.add(new GeoMapNodeDTO(
            username,
            locationMapper.toDto(baseLocation),
            new GeoMapNodeDataDTO()
        ));
        var expectedResponse = new NearbySubscribersDTO(list);
        String expectedResponseJson = new ObjectMapper().writer().writeValueAsString(expectedResponse);

        assertThat(result.getResponse().getContentAsString()).isEqualTo(expectedResponseJson);
    }

    @Test
    @Order(2)
    void unsubscribe() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders.post(UNSUBSCRIBE_URL)
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isOk());

        assertThat(geoMapRepository.getPointsAround(baseLocation, 1).size()).isEqualTo(0);

    }

    @Test
    @Order(3)
    void unsubscribeNotFound() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders.post(UNSUBSCRIBE_URL)
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}
