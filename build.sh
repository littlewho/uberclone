#!/bin/sh

(cd gateway && ./gradlew -Pdev bootJar jibDockerBuild)
(cd profile && ./gradlew -Pdev bootJar jibDockerBuild)
(cd ride && ./gradlew -Pdev bootJar jibDockerBuild)
(cd location && ./gradlew -Pdev bootJar jibDockerBuild)
(cd notification && ./gradlew -Pdev bootJar jibDockerBuild)
(cd payment && ./gradlew -Pdev bootJar jibDockerBuild)

