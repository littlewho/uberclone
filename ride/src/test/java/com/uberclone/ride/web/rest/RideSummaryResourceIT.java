package com.uberclone.ride.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.uberclone.ride.IntegrationTest;
import com.uberclone.ride.domain.RideSummary;
import com.uberclone.ride.domain.enumeration.RideStatus;
import com.uberclone.ride.repository.RideRepository;
import com.uberclone.ride.repository.RideSummaryRepository;
import com.uberclone.ride.service.dto.RideSummaryDTO;
import com.uberclone.ride.service.mapper.RideSummaryMapper;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser(username = RideSummaryResourceIT.DEFAULT_USERNAME)
class RideSummaryResourceIT {
    public static final String DEFAULT_USERNAME = "AAAAAAAAAA";

    private static final RideStatus DEFAULT_STATUS = RideStatus.COMPLETED;
    private static final RideStatus UPDATED_STATUS = RideStatus.CANCELED;

    private static final Integer DEFAULT_STARS = 1;
    private static final Integer UPDATED_STARS = 2;

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/ride-summaries";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private RideRepository rideRepository;

    @Autowired
    private RideSummaryRepository rideSummaryRepository;

    @Autowired
    private RideSummaryMapper rideSummaryMapper;

    @Autowired
    private MockMvc restRideSummaryMockMvc;

    private RideSummary rideSummary;
    public static RideSummary createEntity() {
        return new RideSummary()
            .rideId(DEFAULT_RIDE_ID)
            .status(DEFAULT_STATUS)
            .stars(DEFAULT_STARS)
            .comment(DEFAULT_COMMENT);
    }

    private static String DEFAULT_RIDE_ID;

    @BeforeEach
    public void initTest() {
        rideRepository.deleteAll();
        rideSummaryRepository.deleteAll();

        var parentRide = RideResourceIT.createEntity();
        var result = rideRepository.save(parentRide);
        DEFAULT_RIDE_ID = result.getId();

        rideSummary = createEntity();
    }

    @Test
    void createRideSummary() throws Exception {
        int databaseSizeBeforeCreate = rideSummaryRepository.findAll().size();
        // Create the RideSummary
        RideSummaryDTO rideSummaryDTO = rideSummaryMapper.toDto(rideSummary);
        restRideSummaryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideSummaryDTO))
            )
            .andExpect(status().isCreated());

        // Validate the RideSummary in the database
        List<RideSummary> rideSummaryList = rideSummaryRepository.findAll();
        assertThat(rideSummaryList).hasSize(databaseSizeBeforeCreate + 1);
        RideSummary testRideSummary = rideSummaryList.get(rideSummaryList.size() - 1);
        assertThat(testRideSummary.getRideId()).isEqualTo(DEFAULT_RIDE_ID);
        assertThat(testRideSummary.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testRideSummary.getStars()).isEqualTo(DEFAULT_STARS);
        assertThat(testRideSummary.getComment()).isEqualTo(DEFAULT_COMMENT);
    }

    @Test
    void createRideSummaryWithExistingId() throws Exception {
        // Create the RideSummary with an existing ID
        rideSummary.setId("existing_id");
        RideSummaryDTO rideSummaryDTO = rideSummaryMapper.toDto(rideSummary);

        int databaseSizeBeforeCreate = rideSummaryRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restRideSummaryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideSummaryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RideSummary in the database
        List<RideSummary> rideSummaryList = rideSummaryRepository.findAll();
        assertThat(rideSummaryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkRideIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = rideSummaryRepository.findAll().size();
        // set the field null
        rideSummary.setRideId(null);

        // Create the RideSummary, which fails.
        RideSummaryDTO rideSummaryDTO = rideSummaryMapper.toDto(rideSummary);

        restRideSummaryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideSummaryDTO))
            )
            .andExpect(status().isBadRequest());

        List<RideSummary> rideSummaryList = rideSummaryRepository.findAll();
        assertThat(rideSummaryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = rideSummaryRepository.findAll().size();
        // set the field null
        rideSummary.setStatus(null);

        // Create the RideSummary, which fails.
        RideSummaryDTO rideSummaryDTO = rideSummaryMapper.toDto(rideSummary);

        restRideSummaryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideSummaryDTO))
            )
            .andExpect(status().isBadRequest());

        List<RideSummary> rideSummaryList = rideSummaryRepository.findAll();
        assertThat(rideSummaryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkStarsIsRequired() throws Exception {
        int databaseSizeBeforeTest = rideSummaryRepository.findAll().size();
        // set the field null
        rideSummary.setStars(null);

        // Create the RideSummary, which fails.
        RideSummaryDTO rideSummaryDTO = rideSummaryMapper.toDto(rideSummary);

        restRideSummaryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideSummaryDTO))
            )
            .andExpect(status().isBadRequest());

        List<RideSummary> rideSummaryList = rideSummaryRepository.findAll();
        assertThat(rideSummaryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkCommentIsRequired() throws Exception {
        int databaseSizeBeforeTest = rideSummaryRepository.findAll().size();
        // set the field null
        rideSummary.setComment(null);

        // Create the RideSummary, which fails.
        RideSummaryDTO rideSummaryDTO = rideSummaryMapper.toDto(rideSummary);

        restRideSummaryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideSummaryDTO))
            )
            .andExpect(status().isBadRequest());

        List<RideSummary> rideSummaryList = rideSummaryRepository.findAll();
        assertThat(rideSummaryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllRideSummaries() throws Exception {
        // Initialize the database
        rideSummaryRepository.save(rideSummary);

        // Get all the rideSummaryList
        restRideSummaryMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(rideSummary.getId())))
            .andExpect(jsonPath("$.[*].rideId").value(hasItem(DEFAULT_RIDE_ID)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].stars").value(hasItem(DEFAULT_STARS)))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)));
    }

    @Test
    void getRideSummary() throws Exception {
        // Initialize the database
        rideSummaryRepository.save(rideSummary);

        // Get the rideSummary
        restRideSummaryMockMvc
            .perform(get(ENTITY_API_URL_ID, rideSummary.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(rideSummary.getId()))
            .andExpect(jsonPath("$.rideId").value(DEFAULT_RIDE_ID))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.stars").value(DEFAULT_STARS))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT));
    }

    @Test
    void getNonExistingRideSummary() throws Exception {
        // Get the rideSummary
        restRideSummaryMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putNewRideSummary() throws Exception {
        // Initialize the database
        rideSummaryRepository.save(rideSummary);

        int databaseSizeBeforeUpdate = rideSummaryRepository.findAll().size();

        // Update the rideSummary
        RideSummary updatedRideSummary = rideSummaryRepository.findById(rideSummary.getId()).get();
        updatedRideSummary.status(UPDATED_STATUS).stars(UPDATED_STARS).comment(UPDATED_COMMENT);
        RideSummaryDTO rideSummaryDTO = rideSummaryMapper.toDto(updatedRideSummary);

        restRideSummaryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, rideSummaryDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(rideSummaryDTO))
            )
            .andExpect(status().isOk());

        // Validate the RideSummary in the database
        List<RideSummary> rideSummaryList = rideSummaryRepository.findAll();
        assertThat(rideSummaryList).hasSize(databaseSizeBeforeUpdate);
        RideSummary testRideSummary = rideSummaryList.get(rideSummaryList.size() - 1);
        assertThat(testRideSummary.getRideId()).isEqualTo(DEFAULT_RIDE_ID);
        assertThat(testRideSummary.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testRideSummary.getStars()).isEqualTo(UPDATED_STARS);
        assertThat(testRideSummary.getComment()).isEqualTo(UPDATED_COMMENT);
    }

    @Test
    void putNonExistingRideSummary() throws Exception {
        int databaseSizeBeforeUpdate = rideSummaryRepository.findAll().size();
        rideSummary.setId(UUID.randomUUID().toString());

        // Create the RideSummary
        RideSummaryDTO rideSummaryDTO = rideSummaryMapper.toDto(rideSummary);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRideSummaryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, rideSummaryDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(rideSummaryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RideSummary in the database
        List<RideSummary> rideSummaryList = rideSummaryRepository.findAll();
        assertThat(rideSummaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchRideSummary() throws Exception {
        int databaseSizeBeforeUpdate = rideSummaryRepository.findAll().size();
        rideSummary.setId(UUID.randomUUID().toString());

        // Create the RideSummary
        RideSummaryDTO rideSummaryDTO = rideSummaryMapper.toDto(rideSummary);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRideSummaryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(rideSummaryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RideSummary in the database
        List<RideSummary> rideSummaryList = rideSummaryRepository.findAll();
        assertThat(rideSummaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamRideSummary() throws Exception {
        int databaseSizeBeforeUpdate = rideSummaryRepository.findAll().size();
        rideSummary.setId(UUID.randomUUID().toString());

        // Create the RideSummary
        RideSummaryDTO rideSummaryDTO = rideSummaryMapper.toDto(rideSummary);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRideSummaryMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideSummaryDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the RideSummary in the database
        List<RideSummary> rideSummaryList = rideSummaryRepository.findAll();
        assertThat(rideSummaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateRideSummaryWithPatch() throws Exception {
        // Initialize the database
        rideSummaryRepository.save(rideSummary);

        int databaseSizeBeforeUpdate = rideSummaryRepository.findAll().size();

        // Update the rideSummary using partial update
        RideSummary partialUpdatedRideSummary = new RideSummary();
        partialUpdatedRideSummary.setId(rideSummary.getId());

        partialUpdatedRideSummary.status(UPDATED_STATUS).stars(UPDATED_STARS).comment(UPDATED_COMMENT);

        restRideSummaryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRideSummary.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRideSummary))
            )
            .andExpect(status().isOk());

        // Validate the RideSummary in the database
        List<RideSummary> rideSummaryList = rideSummaryRepository.findAll();
        assertThat(rideSummaryList).hasSize(databaseSizeBeforeUpdate);
        RideSummary testRideSummary = rideSummaryList.get(rideSummaryList.size() - 1);
        assertThat(testRideSummary.getRideId()).isEqualTo(DEFAULT_RIDE_ID);
        assertThat(testRideSummary.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testRideSummary.getStars()).isEqualTo(UPDATED_STARS);
        assertThat(testRideSummary.getComment()).isEqualTo(UPDATED_COMMENT);
    }

    @Test
    void fullUpdateRideSummaryWithPatch() throws Exception {
        // Initialize the database
        rideSummaryRepository.save(rideSummary);

        int databaseSizeBeforeUpdate = rideSummaryRepository.findAll().size();

        // Update the rideSummary using partial update
        RideSummary partialUpdatedRideSummary = new RideSummary();
        partialUpdatedRideSummary.setId(rideSummary.getId());

        partialUpdatedRideSummary.status(UPDATED_STATUS).stars(UPDATED_STARS).comment(UPDATED_COMMENT);

        restRideSummaryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRideSummary.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRideSummary))
            )
            .andExpect(status().isOk());

        // Validate the RideSummary in the database
        List<RideSummary> rideSummaryList = rideSummaryRepository.findAll();
        assertThat(rideSummaryList).hasSize(databaseSizeBeforeUpdate);
        RideSummary testRideSummary = rideSummaryList.get(rideSummaryList.size() - 1);
        assertThat(testRideSummary.getRideId()).isEqualTo(DEFAULT_RIDE_ID);
        assertThat(testRideSummary.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testRideSummary.getStars()).isEqualTo(UPDATED_STARS);
        assertThat(testRideSummary.getComment()).isEqualTo(UPDATED_COMMENT);
    }

    @Test
    void patchNonExistingRideSummary() throws Exception {
        int databaseSizeBeforeUpdate = rideSummaryRepository.findAll().size();
        rideSummary.setId(UUID.randomUUID().toString());

        // Create the RideSummary
        RideSummaryDTO rideSummaryDTO = rideSummaryMapper.toDto(rideSummary);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRideSummaryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, rideSummaryDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(rideSummaryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RideSummary in the database
        List<RideSummary> rideSummaryList = rideSummaryRepository.findAll();
        assertThat(rideSummaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchRideSummary() throws Exception {
        int databaseSizeBeforeUpdate = rideSummaryRepository.findAll().size();
        rideSummary.setId(UUID.randomUUID().toString());

        // Create the RideSummary
        RideSummaryDTO rideSummaryDTO = rideSummaryMapper.toDto(rideSummary);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRideSummaryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(rideSummaryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RideSummary in the database
        List<RideSummary> rideSummaryList = rideSummaryRepository.findAll();
        assertThat(rideSummaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamRideSummary() throws Exception {
        int databaseSizeBeforeUpdate = rideSummaryRepository.findAll().size();
        rideSummary.setId(UUID.randomUUID().toString());

        // Create the RideSummary
        RideSummaryDTO rideSummaryDTO = rideSummaryMapper.toDto(rideSummary);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRideSummaryMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(rideSummaryDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RideSummary in the database
        List<RideSummary> rideSummaryList = rideSummaryRepository.findAll();
        assertThat(rideSummaryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteRideSummary() throws Exception {
        // Initialize the database
        rideSummaryRepository.save(rideSummary);

        int databaseSizeBeforeDelete = rideSummaryRepository.findAll().size();

        // Delete the rideSummary
        restRideSummaryMockMvc
            .perform(delete(ENTITY_API_URL_ID, rideSummary.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RideSummary> rideSummaryList = rideSummaryRepository.findAll();
        assertThat(rideSummaryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
