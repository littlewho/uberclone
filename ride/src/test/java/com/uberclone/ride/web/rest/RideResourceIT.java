package com.uberclone.ride.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.uberclone.ride.IntegrationTest;
import com.uberclone.ride.domain.Ride;
import com.uberclone.ride.repository.RideRepository;
import com.uberclone.ride.service.dto.RideDTO;
import com.uberclone.ride.service.mapper.RideMapper;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser(username = RideResourceIT.DEFAULT_CUSTOMER_USERNAME)
class RideResourceIT {
    public static final String DEFAULT_CUSTOMER_USERNAME = "AAAAAAAAAA";

    private static final String DEFAULT_DRIVER_USERNAME = "AAAAAAAAAA";

    private static final Double DEFAULT_START_LATITUDE = 1D;

    private static final Double DEFAULT_START_LONGITUDE = 1D;

    private static final Double DEFAULT_END_LATITUDE = 1D;

    private static final Double DEFAULT_END_LONGITUDE = 1D;

    private static final Integer DEFAULT_COMFORT = 1;

    private static final Integer DEFAULT_PRICE = 1;

    private static final Integer DEFAULT_TIME = 1;

    private static final Integer DEFAULT_KILOMETERS = 1;

    private static final String ENTITY_API_URL = "/api/rides";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private RideRepository rideRepository;

    @Autowired
    private RideMapper rideMapper;

    @Autowired
    private MockMvc restRideMockMvc;

    private Ride ride;

    public static Ride createEntity() {
        return new Ride()
            .customerUsername(DEFAULT_CUSTOMER_USERNAME)
            .driverUsername(DEFAULT_DRIVER_USERNAME)
            .startLatitude(DEFAULT_START_LATITUDE)
            .startLongitude(DEFAULT_START_LONGITUDE)
            .endLatitude(DEFAULT_END_LATITUDE)
            .endLongitude(DEFAULT_END_LONGITUDE)
            .comfort(DEFAULT_COMFORT)
            .price(DEFAULT_PRICE)
            .time(DEFAULT_TIME)
            .kilometers(DEFAULT_KILOMETERS);
    }

    @BeforeEach
    public void initTest() {
        rideRepository.deleteAll();
        ride = createEntity();
    }

    @Test
    void createRide() throws Exception {
        int databaseSizeBeforeCreate = rideRepository.findAll().size();
        // Create the Ride
        RideDTO rideDTO = rideMapper.toDto(ride);
        restRideMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideDTO)))
            .andExpect(status().isCreated());

        // Validate the Ride in the database
        List<Ride> rideList = rideRepository.findAll();
        assertThat(rideList).hasSize(databaseSizeBeforeCreate + 1);
        Ride testRide = rideList.get(rideList.size() - 1);
        assertThat(testRide.getCustomerUsername()).isEqualTo(DEFAULT_CUSTOMER_USERNAME);
        assertThat(testRide.getDriverUsername()).isEqualTo(DEFAULT_DRIVER_USERNAME);
        assertThat(testRide.getStartLatitude()).isEqualTo(DEFAULT_START_LATITUDE);
        assertThat(testRide.getStartLongitude()).isEqualTo(DEFAULT_START_LONGITUDE);
        assertThat(testRide.getEndLatitude()).isEqualTo(DEFAULT_END_LATITUDE);
        assertThat(testRide.getEndLongitude()).isEqualTo(DEFAULT_END_LONGITUDE);
        assertThat(testRide.getComfort()).isEqualTo(DEFAULT_COMFORT);
        assertThat(testRide.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testRide.getTime()).isEqualTo(DEFAULT_TIME);
        assertThat(testRide.getKilometers()).isEqualTo(DEFAULT_KILOMETERS);
    }

    @Test
    void createRideWithExistingId() throws Exception {
        // Create the Ride with an existing ID
        ride.setId("existing_id");
        RideDTO rideDTO = rideMapper.toDto(ride);

        int databaseSizeBeforeCreate = rideRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restRideMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Ride in the database
        List<Ride> rideList = rideRepository.findAll();
        assertThat(rideList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkCustomerUsernameIsRequired() throws Exception {
        int databaseSizeBeforeTest = rideRepository.findAll().size();
        // set the field null
        ride.setCustomerUsername(null);

        // Create the Ride, which fails.
        RideDTO rideDTO = rideMapper.toDto(ride);

        restRideMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideDTO)))
            .andExpect(status().isBadRequest());

        List<Ride> rideList = rideRepository.findAll();
        assertThat(rideList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkDriverUsernameIsRequired() throws Exception {
        int databaseSizeBeforeTest = rideRepository.findAll().size();
        // set the field null
        ride.setDriverUsername(null);

        // Create the Ride, which fails.
        RideDTO rideDTO = rideMapper.toDto(ride);

        restRideMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideDTO)))
            .andExpect(status().isBadRequest());

        List<Ride> rideList = rideRepository.findAll();
        assertThat(rideList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkStartLatitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = rideRepository.findAll().size();
        // set the field null
        ride.setStartLatitude(null);

        // Create the Ride, which fails.
        RideDTO rideDTO = rideMapper.toDto(ride);

        restRideMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideDTO)))
            .andExpect(status().isBadRequest());

        List<Ride> rideList = rideRepository.findAll();
        assertThat(rideList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkStartLongitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = rideRepository.findAll().size();
        // set the field null
        ride.setStartLongitude(null);

        // Create the Ride, which fails.
        RideDTO rideDTO = rideMapper.toDto(ride);

        restRideMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideDTO)))
            .andExpect(status().isBadRequest());

        List<Ride> rideList = rideRepository.findAll();
        assertThat(rideList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkEndLatitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = rideRepository.findAll().size();
        // set the field null
        ride.setEndLatitude(null);

        // Create the Ride, which fails.
        RideDTO rideDTO = rideMapper.toDto(ride);

        restRideMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideDTO)))
            .andExpect(status().isBadRequest());

        List<Ride> rideList = rideRepository.findAll();
        assertThat(rideList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkEndLongitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = rideRepository.findAll().size();
        // set the field null
        ride.setEndLongitude(null);

        // Create the Ride, which fails.
        RideDTO rideDTO = rideMapper.toDto(ride);

        restRideMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideDTO)))
            .andExpect(status().isBadRequest());

        List<Ride> rideList = rideRepository.findAll();
        assertThat(rideList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkComfortIsRequired() throws Exception {
        int databaseSizeBeforeTest = rideRepository.findAll().size();
        // set the field null
        ride.setComfort(null);

        // Create the Ride, which fails.
        RideDTO rideDTO = rideMapper.toDto(ride);

        restRideMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideDTO)))
            .andExpect(status().isBadRequest());

        List<Ride> rideList = rideRepository.findAll();
        assertThat(rideList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = rideRepository.findAll().size();
        // set the field null
        ride.setPrice(null);

        // Create the Ride, which fails.
        RideDTO rideDTO = rideMapper.toDto(ride);

        restRideMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideDTO)))
            .andExpect(status().isBadRequest());

        List<Ride> rideList = rideRepository.findAll();
        assertThat(rideList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = rideRepository.findAll().size();
        // set the field null
        ride.setTime(null);

        // Create the Ride, which fails.
        RideDTO rideDTO = rideMapper.toDto(ride);

        restRideMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideDTO)))
            .andExpect(status().isBadRequest());

        List<Ride> rideList = rideRepository.findAll();
        assertThat(rideList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkKilometersIsRequired() throws Exception {
        int databaseSizeBeforeTest = rideRepository.findAll().size();
        // set the field null
        ride.setKilometers(null);

        // Create the Ride, which fails.
        RideDTO rideDTO = rideMapper.toDto(ride);

        restRideMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(rideDTO)))
            .andExpect(status().isBadRequest());

        List<Ride> rideList = rideRepository.findAll();
        assertThat(rideList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllRides() throws Exception {
        // Initialize the database
        rideRepository.save(ride);

        // Get all the rideList
        restRideMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ride.getId())))
            .andExpect(jsonPath("$.[*].customerUsername").value(hasItem(DEFAULT_CUSTOMER_USERNAME)))
            .andExpect(jsonPath("$.[*].driverUsername").value(hasItem(DEFAULT_DRIVER_USERNAME)))
            .andExpect(jsonPath("$.[*].startLatitude").value(hasItem(DEFAULT_START_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].startLongitude").value(hasItem(DEFAULT_START_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].endLatitude").value(hasItem(DEFAULT_END_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].endLongitude").value(hasItem(DEFAULT_END_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].comfort").value(hasItem(DEFAULT_COMFORT)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE)))
            .andExpect(jsonPath("$.[*].time").value(hasItem(DEFAULT_TIME)))
            .andExpect(jsonPath("$.[*].kilometers").value(hasItem(DEFAULT_KILOMETERS)));
    }

    @Test
    void getRide() throws Exception {
        // Initialize the database
        rideRepository.save(ride);

        // Get the ride
        restRideMockMvc
            .perform(get(ENTITY_API_URL_ID, ride.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(ride.getId()))
            .andExpect(jsonPath("$.customerUsername").value(DEFAULT_CUSTOMER_USERNAME))
            .andExpect(jsonPath("$.driverUsername").value(DEFAULT_DRIVER_USERNAME))
            .andExpect(jsonPath("$.startLatitude").value(DEFAULT_START_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.startLongitude").value(DEFAULT_START_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.endLatitude").value(DEFAULT_END_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.endLongitude").value(DEFAULT_END_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.comfort").value(DEFAULT_COMFORT))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE))
            .andExpect(jsonPath("$.time").value(DEFAULT_TIME))
            .andExpect(jsonPath("$.kilometers").value(DEFAULT_KILOMETERS));
    }

    @Test
    void getNonExistingRide() throws Exception {
        // Get the ride
        restRideMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }
}
