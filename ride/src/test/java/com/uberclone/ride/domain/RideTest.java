package com.uberclone.ride.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.uberclone.ride.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RideTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Ride.class);
        Ride ride1 = new Ride();
        ride1.setId("id1");
        Ride ride2 = new Ride();
        ride2.setId(ride1.getId());
        assertThat(ride1).isEqualTo(ride2);
        ride2.setId("id2");
        assertThat(ride1).isNotEqualTo(ride2);
        ride1.setId(null);
        assertThat(ride1).isNotEqualTo(ride2);
    }
}
