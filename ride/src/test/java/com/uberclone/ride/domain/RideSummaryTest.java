package com.uberclone.ride.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.uberclone.ride.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RideSummaryTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RideSummary.class);
        RideSummary rideSummary1 = new RideSummary();
        rideSummary1.setId("id1");
        RideSummary rideSummary2 = new RideSummary();
        rideSummary2.setId(rideSummary1.getId());
        assertThat(rideSummary1).isEqualTo(rideSummary2);
        rideSummary2.setId("id2");
        assertThat(rideSummary1).isNotEqualTo(rideSummary2);
        rideSummary1.setId(null);
        assertThat(rideSummary1).isNotEqualTo(rideSummary2);
    }
}
