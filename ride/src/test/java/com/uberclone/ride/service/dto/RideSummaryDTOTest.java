package com.uberclone.ride.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.uberclone.ride.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RideSummaryDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RideSummaryDTO.class);
        RideSummaryDTO rideSummaryDTO1 = new RideSummaryDTO();
        rideSummaryDTO1.setId("id1");
        RideSummaryDTO rideSummaryDTO2 = new RideSummaryDTO();
        assertThat(rideSummaryDTO1).isNotEqualTo(rideSummaryDTO2);
        rideSummaryDTO2.setId(rideSummaryDTO1.getId());
        assertThat(rideSummaryDTO1).isEqualTo(rideSummaryDTO2);
        rideSummaryDTO2.setId("id2");
        assertThat(rideSummaryDTO1).isNotEqualTo(rideSummaryDTO2);
        rideSummaryDTO1.setId(null);
        assertThat(rideSummaryDTO1).isNotEqualTo(rideSummaryDTO2);
    }
}
