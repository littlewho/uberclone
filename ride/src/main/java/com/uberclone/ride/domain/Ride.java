package com.uberclone.ride.domain;

import java.io.Serializable;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A Ride.
 */
@Document(collection = "ride")
public class Ride implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("customer_username")
    private String customerUsername;

    @NotNull
    @Field("driver_username")
    private String driverUsername;

    @NotNull
    @Field("start_latitude")
    private Double startLatitude;

    @NotNull
    @Field("start_longitude")
    private Double startLongitude;

    @NotNull
    @Field("end_latitude")
    private Double endLatitude;

    @NotNull
    @Field("end_longitude")
    private Double endLongitude;

    @NotNull
    @Field("comfort")
    private Integer comfort;

    @NotNull
    @Field("price")
    private Integer price;

    @NotNull
    @Field("time")
    private Integer time;

    @NotNull
    @Field("kilometers")
    private Integer kilometers;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Ride id(String id) {
        this.id = id;
        return this;
    }

    public String getCustomerUsername() {
        return this.customerUsername;
    }

    public Ride customerUsername(String customerUsername) {
        this.customerUsername = customerUsername;
        return this;
    }

    public void setCustomerUsername(String customerUsername) {
        this.customerUsername = customerUsername;
    }

    public String getDriverUsername() {
        return this.driverUsername;
    }

    public Ride driverUsername(String driverUsername) {
        this.driverUsername = driverUsername;
        return this;
    }

    public void setDriverUsername(String driverUsername) {
        this.driverUsername = driverUsername;
    }

    public Double getStartLatitude() {
        return this.startLatitude;
    }

    public Ride startLatitude(Double startLatitude) {
        this.startLatitude = startLatitude;
        return this;
    }

    public void setStartLatitude(Double startLatitude) {
        this.startLatitude = startLatitude;
    }

    public Double getStartLongitude() {
        return this.startLongitude;
    }

    public Ride startLongitude(Double startLongitude) {
        this.startLongitude = startLongitude;
        return this;
    }

    public void setStartLongitude(Double startLongitude) {
        this.startLongitude = startLongitude;
    }

    public Double getEndLatitude() {
        return this.endLatitude;
    }

    public Ride endLatitude(Double endLatitude) {
        this.endLatitude = endLatitude;
        return this;
    }

    public void setEndLatitude(Double endLatitude) {
        this.endLatitude = endLatitude;
    }

    public Double getEndLongitude() {
        return this.endLongitude;
    }

    public Ride endLongitude(Double endLongitude) {
        this.endLongitude = endLongitude;
        return this;
    }

    public void setEndLongitude(Double endLongitude) {
        this.endLongitude = endLongitude;
    }

    public Integer getComfort() {
        return this.comfort;
    }

    public Ride comfort(Integer comfort) {
        this.comfort = comfort;
        return this;
    }

    public void setComfort(Integer comfort) {
        this.comfort = comfort;
    }

    public Integer getPrice() {
        return this.price;
    }

    public Ride price(Integer price) {
        this.price = price;
        return this;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getTime() {
        return this.time;
    }

    public Ride time(Integer time) {
        this.time = time;
        return this;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Integer getKilometers() {
        return this.kilometers;
    }

    public Ride kilometers(Integer kilometers) {
        this.kilometers = kilometers;
        return this;
    }

    public void setKilometers(Integer kilometers) {
        this.kilometers = kilometers;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Ride)) {
            return false;
        }
        return id != null && id.equals(((Ride) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Ride{" +
            "id=" + getId() +
            ", customerUsername='" + getCustomerUsername() + "'" +
            ", driverUsername='" + getDriverUsername() + "'" +
            ", startLatitude=" + getStartLatitude() +
            ", startLongitude=" + getStartLongitude() +
            ", endLatitude=" + getEndLatitude() +
            ", endLongitude=" + getEndLongitude() +
            ", comfort=" + getComfort() +
            ", price=" + getPrice() +
            ", time=" + getTime() +
            ", kilometers=" + getKilometers() +
            "}";
    }
}
