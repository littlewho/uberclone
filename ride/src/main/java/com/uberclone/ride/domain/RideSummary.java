package com.uberclone.ride.domain;

import com.uberclone.ride.domain.enumeration.RideStatus;
import java.io.Serializable;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A RideSummary.
 */
@Document(collection = "ride_summary")
public class RideSummary implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("ride_id")
    private String rideId;

    @NotNull
    @Field("status")
    private RideStatus status;

    @NotNull
    @Field("stars")
    private Integer stars;

    @NotNull
    @Field("comment")
    private String comment;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RideSummary id(String id) {
        this.id = id;
        return this;
    }

    public String getRideId() {
        return this.rideId;
    }

    public RideSummary rideId(String rideId) {
        this.rideId = rideId;
        return this;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public RideStatus getStatus() {
        return this.status;
    }

    public RideSummary status(RideStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(RideStatus status) {
        this.status = status;
    }

    public Integer getStars() {
        return this.stars;
    }

    public RideSummary stars(Integer stars) {
        this.stars = stars;
        return this;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public String getComment() {
        return this.comment;
    }

    public RideSummary comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RideSummary)) {
            return false;
        }
        return id != null && id.equals(((RideSummary) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RideSummary{" +
            "id=" + getId() +
            ", rideId='" + getRideId() + "'" +
            ", status='" + getStatus() + "'" +
            ", stars=" + getStars() +
            ", comment='" + getComment() + "'" +
            "}";
    }
}
