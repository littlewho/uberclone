package com.uberclone.ride.domain.enumeration;

/**
 * The RideStatus enumeration.
 */
public enum RideStatus {
    COMPLETED,
    CANCELED,
    ABUSE_REPORT,
}
