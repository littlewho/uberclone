package com.uberclone.ride.web.rest;

import com.uberclone.ride.service.RideService;
import com.uberclone.ride.service.dto.RideDTO;
import com.uberclone.ride.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api")
public class RideResource {
    private final Logger log = LoggerFactory.getLogger(RideResource.class);
    private static final String ENTITY_NAME = "rideRide";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RideService rideService;

    public RideResource(RideService rideService) {
        this.rideService = rideService;
    }

    @PostMapping("/rides")
    public ResponseEntity<RideDTO> createRide(@Valid @RequestBody RideDTO rideDTO) throws URISyntaxException {
        log.debug("REST request to save Ride : {}", rideDTO);
        if (rideDTO.getId() != null) {
            throw new BadRequestAlertException("A new ride cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RideDTO result = rideService.save(rideDTO);
        return ResponseEntity
            .created(new URI("/api/rides/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @GetMapping("/rides")
    public List<RideDTO> getAllRides() {
        log.debug("REST request to get all Rides");
        return rideService.findAll();
    }

    @GetMapping("/rides/{id}")
    public ResponseEntity<RideDTO> getRide(@PathVariable String id) {
        log.debug("REST request to get Ride : {}", id);
        Optional<RideDTO> rideDTO = rideService.findOne(id);
        return ResponseUtil.wrapOrNotFound(rideDTO);
    }
}
