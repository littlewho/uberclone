package com.uberclone.ride.web.rest;

import com.uberclone.ride.repository.RideSummaryRepository;
import com.uberclone.ride.service.RideService;
import com.uberclone.ride.service.RideSummaryService;
import com.uberclone.ride.service.dto.RideSummaryDTO;
import com.uberclone.ride.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api")
public class RideSummaryResource {
    private final Logger log = LoggerFactory.getLogger(RideSummaryResource.class);
    private static final String ENTITY_NAME = "rideRideSummary";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RideSummaryService rideSummaryService;
    private final RideService rideService;

    public RideSummaryResource(RideSummaryService rideSummaryService, RideService rideService) {
        this.rideSummaryService = rideSummaryService;
        this.rideService = rideService;
    }

    @PostMapping("/ride-summaries")
    public ResponseEntity<RideSummaryDTO> createRideSummary(
        Principal principal,
        @Valid @RequestBody RideSummaryDTO rideSummaryDTO) throws URISyntaxException {
        log.debug("REST request to save RideSummary : {}", rideSummaryDTO);
        if (rideSummaryDTO.getId() != null) {
            throw new BadRequestAlertException("A new rideSummary cannot already have an ID", ENTITY_NAME, "idexists");
        }

        validateParent(principal, rideSummaryDTO);

        RideSummaryDTO result = rideSummaryService.save(rideSummaryDTO);
        return ResponseEntity
            .created(new URI("/api/ride-summaries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @PutMapping("/ride-summaries/{id}")
    public ResponseEntity<RideSummaryDTO> updateRideSummary(
        Principal principal,
        @PathVariable(value = "id", required = false) final String id,
        @Valid @RequestBody RideSummaryDTO rideSummaryDTO
    ) {
        log.debug("REST request to update RideSummary : {}, {}", id, rideSummaryDTO);

        validateForUpdate(id, rideSummaryDTO);
        validateParent(principal, rideSummaryDTO);

        RideSummaryDTO result = rideSummaryService.save(rideSummaryDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, rideSummaryDTO.getId()))
            .body(result);
    }

    @PatchMapping(value = "/ride-summaries/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<RideSummaryDTO> partialUpdateRideSummary(
        Principal principal,
        @PathVariable(value = "id", required = false) final String id,
        @NotNull @RequestBody RideSummaryDTO rideSummaryDTO
    ) {
        log.debug("REST request to partial update RideSummary partially : {}, {}", id, rideSummaryDTO);

        validateForUpdate(id, rideSummaryDTO);
        validateParent(principal, rideSummaryDTO);

        Optional<RideSummaryDTO> result = rideSummaryService.partialUpdate(rideSummaryDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, rideSummaryDTO.getId())
        );
    }

    @GetMapping("/ride-summaries")
    public List<RideSummaryDTO> getAllRideSummaries() {
        log.debug("REST request to get all RideSummaries");
        return rideSummaryService.findAll();
    }

    @GetMapping("/ride-summaries/{id}")
    public ResponseEntity<RideSummaryDTO> getRideSummary(@PathVariable String id) {
        log.debug("REST request to get RideSummary : {}", id);
        Optional<RideSummaryDTO> rideSummaryDTO = rideSummaryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(rideSummaryDTO);
    }

    @DeleteMapping("/ride-summaries/{id}")
    public ResponseEntity<Void> deleteRideSummary(@PathVariable String id) {
        log.debug("REST request to delete RideSummary : {}", id);
        rideSummaryService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }

    private void validateParent(Principal principal, RideSummaryDTO rideSummaryDTO) {
        var parentOpt = rideService.findOne(rideSummaryDTO.getRideId());
        if (parentOpt.isEmpty()) {
            throw new BadRequestAlertException("Could not find associated ride", ENTITY_NAME, "parentnotfound");
        }

        var parent = parentOpt.get();
        if (!parent.getCustomerUsername().equals(principal.getName())) {
            throw new BadRequestAlertException("You are not the customer of this ride", ENTITY_NAME, "forbidden");
        }
    }

    private void validateForUpdate(String id, RideSummaryDTO rideSummaryDTO) {
        if (rideSummaryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, rideSummaryDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        var existingSummaryOpt = rideSummaryService.findOne(id);
        if (existingSummaryOpt.isEmpty()) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        var existingSummary = existingSummaryOpt.get();
        if (rideSummaryDTO.getRideId() != null && !existingSummary.getRideId().equals(rideSummaryDTO.getRideId())) {
            throw new BadRequestAlertException("Can't update parent id", ENTITY_NAME, "parentreadonly");
        }
        rideSummaryDTO.setRideId(existingSummary.getRideId());
    }
}
