/**
 * View Models used by Spring MVC REST controllers.
 */
package com.uberclone.ride.web.rest.vm;
