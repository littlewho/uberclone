package com.uberclone.ride.repository;

import com.uberclone.ride.domain.Ride;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Ride entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RideRepository extends MongoRepository<Ride, String> {}
