package com.uberclone.ride.repository;

import com.uberclone.ride.domain.RideSummary;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the RideSummary entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RideSummaryRepository extends MongoRepository<RideSummary, String> {}
