package com.uberclone.ride.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.uberclone.ride.domain.Ride} entity.
 */
public class RideDTO implements Serializable {

    private String id;

    @NotNull
    private String customerUsername;

    @NotNull
    private String driverUsername;

    @NotNull
    private Double startLatitude;

    @NotNull
    private Double startLongitude;

    @NotNull
    private Double endLatitude;

    @NotNull
    private Double endLongitude;

    @NotNull
    private Integer comfort;

    @NotNull
    private Integer price;

    @NotNull
    private Integer time;

    @NotNull
    private Integer kilometers;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerUsername() {
        return customerUsername;
    }

    public void setCustomerUsername(String customerUsername) {
        this.customerUsername = customerUsername;
    }

    public String getDriverUsername() {
        return driverUsername;
    }

    public void setDriverUsername(String driverUsername) {
        this.driverUsername = driverUsername;
    }

    public Double getStartLatitude() {
        return startLatitude;
    }

    public void setStartLatitude(Double startLatitude) {
        this.startLatitude = startLatitude;
    }

    public Double getStartLongitude() {
        return startLongitude;
    }

    public void setStartLongitude(Double startLongitude) {
        this.startLongitude = startLongitude;
    }

    public Double getEndLatitude() {
        return endLatitude;
    }

    public void setEndLatitude(Double endLatitude) {
        this.endLatitude = endLatitude;
    }

    public Double getEndLongitude() {
        return endLongitude;
    }

    public void setEndLongitude(Double endLongitude) {
        this.endLongitude = endLongitude;
    }

    public Integer getComfort() {
        return comfort;
    }

    public void setComfort(Integer comfort) {
        this.comfort = comfort;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Integer getKilometers() {
        return kilometers;
    }

    public void setKilometers(Integer kilometers) {
        this.kilometers = kilometers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RideDTO)) {
            return false;
        }

        RideDTO rideDTO = (RideDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, rideDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RideDTO{" +
            "id='" + getId() + "'" +
            ", customerUsername='" + getCustomerUsername() + "'" +
            ", driverUsername='" + getDriverUsername() + "'" +
            ", startLatitude=" + getStartLatitude() +
            ", startLongitude=" + getStartLongitude() +
            ", endLatitude=" + getEndLatitude() +
            ", endLongitude=" + getEndLongitude() +
            ", comfort=" + getComfort() +
            ", price=" + getPrice() +
            ", time=" + getTime() +
            ", kilometers=" + getKilometers() +
            "}";
    }
}
