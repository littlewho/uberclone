package com.uberclone.ride.service.dto;

import com.uberclone.ride.domain.enumeration.RideStatus;
import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.uberclone.ride.domain.RideSummary} entity.
 */
public class RideSummaryDTO implements Serializable {

    private String id;

    @NotNull
    private String rideId;

    @NotNull
    private RideStatus status;

    @NotNull
    private Integer stars;

    @NotNull
    private String comment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public RideStatus getStatus() {
        return status;
    }

    public void setStatus(RideStatus status) {
        this.status = status;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RideSummaryDTO)) {
            return false;
        }

        RideSummaryDTO rideSummaryDTO = (RideSummaryDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, rideSummaryDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RideSummaryDTO{" +
            "id='" + getId() + "'" +
            ", rideId='" + getRideId() + "'" +
            ", status='" + getStatus() + "'" +
            ", stars=" + getStars() +
            ", comment='" + getComment() + "'" +
            "}";
    }
}
