package com.uberclone.ride.service;

import com.uberclone.ride.domain.RideSummary;
import com.uberclone.ride.repository.RideRepository;
import com.uberclone.ride.repository.RideSummaryRepository;
import com.uberclone.ride.service.dto.RideDTO;
import com.uberclone.ride.service.dto.RideSummaryDTO;
import com.uberclone.ride.service.mapper.RideMapper;
import com.uberclone.ride.service.mapper.RideSummaryMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class RideSummaryService {
    private final Logger log = LoggerFactory.getLogger(RideSummaryService.class);

    private final RideSummaryRepository rideSummaryRepository;
    private final RideRepository rideRepository;
    private final RideSummaryMapper rideSummaryMapper;
    private final RideMapper rideMapper;

    public RideSummaryService(RideSummaryRepository rideSummaryRepository, RideRepository rideRepository, RideSummaryMapper rideSummaryMapper, RideMapper rideMapper) {
        this.rideSummaryRepository = rideSummaryRepository;
        this.rideRepository = rideRepository;
        this.rideSummaryMapper = rideSummaryMapper;
        this.rideMapper = rideMapper;
    }

    public RideSummaryDTO save(RideSummaryDTO rideSummaryDTO) {
        log.debug("Request to save RideSummary : {}", rideSummaryDTO);
        RideSummary rideSummary = rideSummaryMapper.toEntity(rideSummaryDTO);
        rideSummary = rideSummaryRepository.save(rideSummary);
        return rideSummaryMapper.toDto(rideSummary);
    }

    public Optional<RideSummaryDTO> partialUpdate(RideSummaryDTO rideSummaryDTO) {
        log.debug("Request to partially update RideSummary : {}", rideSummaryDTO);

        return rideSummaryRepository
            .findById(rideSummaryDTO.getId())
            .map(
                existingRideSummary -> {
                    rideSummaryMapper.partialUpdate(existingRideSummary, rideSummaryDTO);
                    return existingRideSummary;
                }
            )
            .map(rideSummaryRepository::save)
            .map(rideSummaryMapper::toDto);
    }

    public List<RideSummaryDTO> findAll() {
        log.debug("Request to get all RideSummaries");
        return rideSummaryRepository.findAll().stream().map(rideSummaryMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    public Optional<RideSummaryDTO> findOne(String id) {
        log.debug("Request to get RideSummary : {}", id);
        return rideSummaryRepository.findById(id).map(rideSummaryMapper::toDto);
    }

    public void delete(String id) {
        log.debug("Request to delete RideSummary : {}", id);
        rideSummaryRepository.deleteById(id);
    }

    public Optional<RideDTO> findParentRide(@NonNull RideSummaryDTO rideSummaryDTO) {
        return rideRepository.findById(rideSummaryDTO.getRideId()).map(rideMapper::toDto);
    }
}
