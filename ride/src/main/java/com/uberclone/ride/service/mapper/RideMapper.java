package com.uberclone.ride.service.mapper;

import com.uberclone.ride.domain.*;
import com.uberclone.ride.service.dto.RideDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Ride} and its DTO {@link RideDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RideMapper extends EntityMapper<RideDTO, Ride> {}
