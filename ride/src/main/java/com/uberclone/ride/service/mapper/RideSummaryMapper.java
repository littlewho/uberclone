package com.uberclone.ride.service.mapper;

import com.uberclone.ride.domain.*;
import com.uberclone.ride.service.dto.RideSummaryDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link RideSummary} and its DTO {@link RideSummaryDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RideSummaryMapper extends EntityMapper<RideSummaryDTO, RideSummary> {}
