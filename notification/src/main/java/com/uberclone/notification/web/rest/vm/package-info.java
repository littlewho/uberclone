/**
 * View Models used by Spring MVC REST controllers.
 */
package com.uberclone.notification.web.rest.vm;
