application {
  config {
    baseName gateway,
    packageName com.uberclone.gateway,
    applicationType gateway,
    authenticationType jwt,
    prodDatabaseType postgresql,
    serverPort 8080,
    serviceDiscoveryType eureka,
    clientFramework react,
    reactive true,
    buildTool gradle
  }
  entities *
}

application {
    config {
        baseName profile,
        packageName com.uberclone.profile,
        applicationType microservice,
        authenticationType jwt,
        databaseType mongodb,
        prodDatabaseType mongodb,
        serverPort 8081,
        serviceDiscoveryType eureka,
        skipUserManagement true,
        buildTool gradle
    }
    entities UserProfile, Car
}

application {
    config {
        baseName ride,
        packageName com.uberclone.ride,
        applicationType microservice,
        authenticationType jwt,
        databaseType mongodb,
        prodDatabaseType mongodb,
        serverPort 8082,
        serviceDiscoveryType eureka,
        skipUserManagement true,
        buildTool gradle
    }
    entities Ride, RideSummary
}

application {
    config {
        baseName location,
        packageName com.uberclone.location,
        applicationType microservice,
        authenticationType jwt,
        databaseType no,
        websocket spring-websocket,
        serverPort 8083,
        serviceDiscoveryType eureka,
        skipUserManagement true,
        buildTool gradle
    }
}

application {
    config {
        baseName payment,
        packageName com.uberclone.payment,
        applicationType microservice,
        authenticationType jwt,
        databaseType sql,
        prodDatabaseType postgresql,
        serverPort 8084,
        serviceDiscoveryType eureka,
        skipUserManagement true,
        buildTool gradle
    }
    entities BankAccount, PaymentOrder
}

application {
    config {
        baseName notification
        packageName com.uberclone.notification,
        applicationType microservice,
        authenticationType jwt,
        databaseType no,
        websocket spring-websocket,
        serverPort 8085,
        serviceDiscoveryType eureka,
        skipUserManagement true,
        buildTool gradle
    }
}

entity UserProfile {
    username String required unique,

    isDriver Boolean required,
}

entity Car {
    username String required,

    model String required,
    color String required,
    comfort Integer required,
}

entity Ride {
    customerUsername String required,
    driverUsername String required,

    startLatitude Double required,
    startLongitude Double required,

    endLatitude Double required,
    endLongitude Double required,

    comfort Integer required,
    price Integer required,
    time Integer required,
    kilometers Integer required,
}

enum RideStatus {
	COMPLETED, CANCELED, ABUSE_REPORT
}

entity RideSummary {
    rideId String required unique,

    status RideStatus required,
    stars Integer required,
    comment String required,
}

entity BankAccount {
    username String required unique,

    amount Integer required,
}

entity PaymentOrder {
    rideId String required unique,

    amount Integer required,
}

relationship OneToMany {
    BankAccount{outgoingPayments} to PaymentOrder{fromAccount required}
    BankAccount{incomingPayments} to PaymentOrder{toAccount required}
}

microservice UserProfile, Car with profile
microservice Ride, RideSummary with ride
microservice BankAccount, PaymentOrder with payment 

dto * with mapstruct 
service * with serviceClass

deployment {
    deploymentType docker-compose
    dockerRepositoryName "thelittlewho"
    appsFolders [gateway, profile, ride, location, payment, notification]
}

