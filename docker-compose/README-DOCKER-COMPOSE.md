# JHipster generated Docker-Compose configuration

## Usage

Launch all your infrastructure by running: `docker-compose up -d`.

## Configured Docker services

### Service registry and configuration server:

- [JHipster Registry](http://localhost:8761)

### Applications and dependencies:

- gateway (gateway application)
- gateway's postgresql database
- profile (microservice application)
- profile's mongodb database
- ride (microservice application)
- ride's mongodb database
- location (microservice application)
- location's no database
- payment (microservice application)
- payment's postgresql database
- notification (microservice application)
- notification's no database

### Additional Services:
