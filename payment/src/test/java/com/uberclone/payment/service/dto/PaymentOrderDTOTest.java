package com.uberclone.payment.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.uberclone.payment.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PaymentOrderDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PaymentOrderDTO.class);
        PaymentOrderDTO paymentOrderDTO1 = new PaymentOrderDTO();
        paymentOrderDTO1.setId(1L);
        PaymentOrderDTO paymentOrderDTO2 = new PaymentOrderDTO();
        assertThat(paymentOrderDTO1).isNotEqualTo(paymentOrderDTO2);
        paymentOrderDTO2.setId(paymentOrderDTO1.getId());
        assertThat(paymentOrderDTO1).isEqualTo(paymentOrderDTO2);
        paymentOrderDTO2.setId(2L);
        assertThat(paymentOrderDTO1).isNotEqualTo(paymentOrderDTO2);
        paymentOrderDTO1.setId(null);
        assertThat(paymentOrderDTO1).isNotEqualTo(paymentOrderDTO2);
    }
}
