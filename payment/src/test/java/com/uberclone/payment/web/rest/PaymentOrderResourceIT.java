package com.uberclone.payment.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.uberclone.payment.IntegrationTest;
import com.uberclone.payment.domain.BankAccount;
import com.uberclone.payment.domain.PaymentOrder;
import com.uberclone.payment.repository.PaymentOrderRepository;
import com.uberclone.payment.service.dto.PaymentOrderDTO;
import com.uberclone.payment.service.mapper.PaymentOrderMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PaymentOrderResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PaymentOrderResourceIT {

    private static final String DEFAULT_RIDE_ID = "AAAAAAAAAA";
    private static final String UPDATED_RIDE_ID = "BBBBBBBBBB";

    private static final Integer DEFAULT_AMOUNT = 1;
    private static final Integer UPDATED_AMOUNT = 2;

    private static final String ENTITY_API_URL = "/api/payment-orders";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PaymentOrderRepository paymentOrderRepository;

    @Autowired
    private PaymentOrderMapper paymentOrderMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPaymentOrderMockMvc;

    private PaymentOrder paymentOrder;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PaymentOrder createEntity(EntityManager em) {
        PaymentOrder paymentOrder = new PaymentOrder().rideId(DEFAULT_RIDE_ID).amount(DEFAULT_AMOUNT);
        // Add required entity
        BankAccount bankAccount;
        if (TestUtil.findAll(em, BankAccount.class).isEmpty()) {
            bankAccount = BankAccountResourceIT.createEntity(em);
            em.persist(bankAccount);
            em.flush();
        } else {
            bankAccount = TestUtil.findAll(em, BankAccount.class).get(0);
        }
        paymentOrder.setFromAccount(bankAccount);
        // Add required entity
        paymentOrder.setToAccount(bankAccount);
        return paymentOrder;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PaymentOrder createUpdatedEntity(EntityManager em) {
        PaymentOrder paymentOrder = new PaymentOrder().rideId(UPDATED_RIDE_ID).amount(UPDATED_AMOUNT);
        // Add required entity
        BankAccount bankAccount;
        if (TestUtil.findAll(em, BankAccount.class).isEmpty()) {
            bankAccount = BankAccountResourceIT.createUpdatedEntity(em);
            em.persist(bankAccount);
            em.flush();
        } else {
            bankAccount = TestUtil.findAll(em, BankAccount.class).get(0);
        }
        paymentOrder.setFromAccount(bankAccount);
        // Add required entity
        paymentOrder.setToAccount(bankAccount);
        return paymentOrder;
    }

    @BeforeEach
    public void initTest() {
        paymentOrder = createEntity(em);
    }

    @Test
    @Transactional
    void createPaymentOrder() throws Exception {
        int databaseSizeBeforeCreate = paymentOrderRepository.findAll().size();
        // Create the PaymentOrder
        PaymentOrderDTO paymentOrderDTO = paymentOrderMapper.toDto(paymentOrder);
        restPaymentOrderMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO))
            )
            .andExpect(status().isCreated());

        // Validate the PaymentOrder in the database
        List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
        assertThat(paymentOrderList).hasSize(databaseSizeBeforeCreate + 1);
        PaymentOrder testPaymentOrder = paymentOrderList.get(paymentOrderList.size() - 1);
        assertThat(testPaymentOrder.getRideId()).isEqualTo(DEFAULT_RIDE_ID);
        assertThat(testPaymentOrder.getAmount()).isEqualTo(DEFAULT_AMOUNT);
    }

    @Test
    @Transactional
    void createPaymentOrderWithExistingId() throws Exception {
        // Create the PaymentOrder with an existing ID
        paymentOrder.setId(1L);
        PaymentOrderDTO paymentOrderDTO = paymentOrderMapper.toDto(paymentOrder);

        int databaseSizeBeforeCreate = paymentOrderRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPaymentOrderMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PaymentOrder in the database
        List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
        assertThat(paymentOrderList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkRideIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentOrderRepository.findAll().size();
        // set the field null
        paymentOrder.setRideId(null);

        // Create the PaymentOrder, which fails.
        PaymentOrderDTO paymentOrderDTO = paymentOrderMapper.toDto(paymentOrder);

        restPaymentOrderMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO))
            )
            .andExpect(status().isBadRequest());

        List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
        assertThat(paymentOrderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentOrderRepository.findAll().size();
        // set the field null
        paymentOrder.setAmount(null);

        // Create the PaymentOrder, which fails.
        PaymentOrderDTO paymentOrderDTO = paymentOrderMapper.toDto(paymentOrder);

        restPaymentOrderMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO))
            )
            .andExpect(status().isBadRequest());

        List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
        assertThat(paymentOrderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPaymentOrders() throws Exception {
        // Initialize the database
        paymentOrderRepository.saveAndFlush(paymentOrder);

        // Get all the paymentOrderList
        restPaymentOrderMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(paymentOrder.getId().intValue())))
            .andExpect(jsonPath("$.[*].rideId").value(hasItem(DEFAULT_RIDE_ID)))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)));
    }

    @Test
    @Transactional
    void getPaymentOrder() throws Exception {
        // Initialize the database
        paymentOrderRepository.saveAndFlush(paymentOrder);

        // Get the paymentOrder
        restPaymentOrderMockMvc
            .perform(get(ENTITY_API_URL_ID, paymentOrder.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(paymentOrder.getId().intValue()))
            .andExpect(jsonPath("$.rideId").value(DEFAULT_RIDE_ID))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT));
    }

    @Test
    @Transactional
    void getNonExistingPaymentOrder() throws Exception {
        // Get the paymentOrder
        restPaymentOrderMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPaymentOrder() throws Exception {
        // Initialize the database
        paymentOrderRepository.saveAndFlush(paymentOrder);

        int databaseSizeBeforeUpdate = paymentOrderRepository.findAll().size();

        // Update the paymentOrder
        PaymentOrder updatedPaymentOrder = paymentOrderRepository.findById(paymentOrder.getId()).get();
        // Disconnect from session so that the updates on updatedPaymentOrder are not directly saved in db
        em.detach(updatedPaymentOrder);
        updatedPaymentOrder.rideId(UPDATED_RIDE_ID).amount(UPDATED_AMOUNT);
        PaymentOrderDTO paymentOrderDTO = paymentOrderMapper.toDto(updatedPaymentOrder);

        restPaymentOrderMockMvc
            .perform(
                put(ENTITY_API_URL_ID, paymentOrderDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO))
            )
            .andExpect(status().isOk());

        // Validate the PaymentOrder in the database
        List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
        assertThat(paymentOrderList).hasSize(databaseSizeBeforeUpdate);
        PaymentOrder testPaymentOrder = paymentOrderList.get(paymentOrderList.size() - 1);
        assertThat(testPaymentOrder.getRideId()).isEqualTo(UPDATED_RIDE_ID);
        assertThat(testPaymentOrder.getAmount()).isEqualTo(UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void putNonExistingPaymentOrder() throws Exception {
        int databaseSizeBeforeUpdate = paymentOrderRepository.findAll().size();
        paymentOrder.setId(count.incrementAndGet());

        // Create the PaymentOrder
        PaymentOrderDTO paymentOrderDTO = paymentOrderMapper.toDto(paymentOrder);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPaymentOrderMockMvc
            .perform(
                put(ENTITY_API_URL_ID, paymentOrderDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PaymentOrder in the database
        List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
        assertThat(paymentOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPaymentOrder() throws Exception {
        int databaseSizeBeforeUpdate = paymentOrderRepository.findAll().size();
        paymentOrder.setId(count.incrementAndGet());

        // Create the PaymentOrder
        PaymentOrderDTO paymentOrderDTO = paymentOrderMapper.toDto(paymentOrder);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentOrderMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PaymentOrder in the database
        List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
        assertThat(paymentOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPaymentOrder() throws Exception {
        int databaseSizeBeforeUpdate = paymentOrderRepository.findAll().size();
        paymentOrder.setId(count.incrementAndGet());

        // Create the PaymentOrder
        PaymentOrderDTO paymentOrderDTO = paymentOrderMapper.toDto(paymentOrder);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentOrderMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PaymentOrder in the database
        List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
        assertThat(paymentOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePaymentOrderWithPatch() throws Exception {
        // Initialize the database
        paymentOrderRepository.saveAndFlush(paymentOrder);

        int databaseSizeBeforeUpdate = paymentOrderRepository.findAll().size();

        // Update the paymentOrder using partial update
        PaymentOrder partialUpdatedPaymentOrder = new PaymentOrder();
        partialUpdatedPaymentOrder.setId(paymentOrder.getId());

        restPaymentOrderMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPaymentOrder.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPaymentOrder))
            )
            .andExpect(status().isOk());

        // Validate the PaymentOrder in the database
        List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
        assertThat(paymentOrderList).hasSize(databaseSizeBeforeUpdate);
        PaymentOrder testPaymentOrder = paymentOrderList.get(paymentOrderList.size() - 1);
        assertThat(testPaymentOrder.getRideId()).isEqualTo(DEFAULT_RIDE_ID);
        assertThat(testPaymentOrder.getAmount()).isEqualTo(DEFAULT_AMOUNT);
    }

    @Test
    @Transactional
    void fullUpdatePaymentOrderWithPatch() throws Exception {
        // Initialize the database
        paymentOrderRepository.saveAndFlush(paymentOrder);

        int databaseSizeBeforeUpdate = paymentOrderRepository.findAll().size();

        // Update the paymentOrder using partial update
        PaymentOrder partialUpdatedPaymentOrder = new PaymentOrder();
        partialUpdatedPaymentOrder.setId(paymentOrder.getId());

        partialUpdatedPaymentOrder.rideId(UPDATED_RIDE_ID).amount(UPDATED_AMOUNT);

        restPaymentOrderMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPaymentOrder.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPaymentOrder))
            )
            .andExpect(status().isOk());

        // Validate the PaymentOrder in the database
        List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
        assertThat(paymentOrderList).hasSize(databaseSizeBeforeUpdate);
        PaymentOrder testPaymentOrder = paymentOrderList.get(paymentOrderList.size() - 1);
        assertThat(testPaymentOrder.getRideId()).isEqualTo(UPDATED_RIDE_ID);
        assertThat(testPaymentOrder.getAmount()).isEqualTo(UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void patchNonExistingPaymentOrder() throws Exception {
        int databaseSizeBeforeUpdate = paymentOrderRepository.findAll().size();
        paymentOrder.setId(count.incrementAndGet());

        // Create the PaymentOrder
        PaymentOrderDTO paymentOrderDTO = paymentOrderMapper.toDto(paymentOrder);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPaymentOrderMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, paymentOrderDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PaymentOrder in the database
        List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
        assertThat(paymentOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPaymentOrder() throws Exception {
        int databaseSizeBeforeUpdate = paymentOrderRepository.findAll().size();
        paymentOrder.setId(count.incrementAndGet());

        // Create the PaymentOrder
        PaymentOrderDTO paymentOrderDTO = paymentOrderMapper.toDto(paymentOrder);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentOrderMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PaymentOrder in the database
        List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
        assertThat(paymentOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPaymentOrder() throws Exception {
        int databaseSizeBeforeUpdate = paymentOrderRepository.findAll().size();
        paymentOrder.setId(count.incrementAndGet());

        // Create the PaymentOrder
        PaymentOrderDTO paymentOrderDTO = paymentOrderMapper.toDto(paymentOrder);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentOrderMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PaymentOrder in the database
        List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
        assertThat(paymentOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePaymentOrder() throws Exception {
        // Initialize the database
        paymentOrderRepository.saveAndFlush(paymentOrder);

        int databaseSizeBeforeDelete = paymentOrderRepository.findAll().size();

        // Delete the paymentOrder
        restPaymentOrderMockMvc
            .perform(delete(ENTITY_API_URL_ID, paymentOrder.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
        assertThat(paymentOrderList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
