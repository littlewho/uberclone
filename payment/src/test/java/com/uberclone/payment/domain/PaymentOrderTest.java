package com.uberclone.payment.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.uberclone.payment.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PaymentOrderTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PaymentOrder.class);
        PaymentOrder paymentOrder1 = new PaymentOrder();
        paymentOrder1.setId(1L);
        PaymentOrder paymentOrder2 = new PaymentOrder();
        paymentOrder2.setId(paymentOrder1.getId());
        assertThat(paymentOrder1).isEqualTo(paymentOrder2);
        paymentOrder2.setId(2L);
        assertThat(paymentOrder1).isNotEqualTo(paymentOrder2);
        paymentOrder1.setId(null);
        assertThat(paymentOrder1).isNotEqualTo(paymentOrder2);
    }
}
