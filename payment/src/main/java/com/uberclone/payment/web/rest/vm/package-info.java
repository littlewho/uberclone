/**
 * View Models used by Spring MVC REST controllers.
 */
package com.uberclone.payment.web.rest.vm;
