package com.uberclone.payment.web.rest;

import com.uberclone.payment.repository.PaymentOrderRepository;
import com.uberclone.payment.service.PaymentOrderService;
import com.uberclone.payment.service.dto.PaymentOrderDTO;
import com.uberclone.payment.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.uberclone.payment.domain.PaymentOrder}.
 */
@RestController
@RequestMapping("/api")
public class PaymentOrderResource {

    private final Logger log = LoggerFactory.getLogger(PaymentOrderResource.class);

    private static final String ENTITY_NAME = "paymentPaymentOrder";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PaymentOrderService paymentOrderService;

    private final PaymentOrderRepository paymentOrderRepository;

    public PaymentOrderResource(PaymentOrderService paymentOrderService, PaymentOrderRepository paymentOrderRepository) {
        this.paymentOrderService = paymentOrderService;
        this.paymentOrderRepository = paymentOrderRepository;
    }

    /**
     * {@code POST  /payment-orders} : Create a new paymentOrder.
     *
     * @param paymentOrderDTO the paymentOrderDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new paymentOrderDTO, or with status {@code 400 (Bad Request)} if the paymentOrder has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/payment-orders")
    public ResponseEntity<PaymentOrderDTO> createPaymentOrder(@Valid @RequestBody PaymentOrderDTO paymentOrderDTO)
        throws URISyntaxException {
        log.debug("REST request to save PaymentOrder : {}", paymentOrderDTO);
        if (paymentOrderDTO.getId() != null) {
            throw new BadRequestAlertException("A new paymentOrder cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PaymentOrderDTO result = paymentOrderService.save(paymentOrderDTO);
        return ResponseEntity
            .created(new URI("/api/payment-orders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /payment-orders/:id} : Updates an existing paymentOrder.
     *
     * @param id the id of the paymentOrderDTO to save.
     * @param paymentOrderDTO the paymentOrderDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated paymentOrderDTO,
     * or with status {@code 400 (Bad Request)} if the paymentOrderDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the paymentOrderDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/payment-orders/{id}")
    public ResponseEntity<PaymentOrderDTO> updatePaymentOrder(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PaymentOrderDTO paymentOrderDTO
    ) throws URISyntaxException {
        log.debug("REST request to update PaymentOrder : {}, {}", id, paymentOrderDTO);
        if (paymentOrderDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, paymentOrderDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!paymentOrderRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PaymentOrderDTO result = paymentOrderService.save(paymentOrderDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, paymentOrderDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /payment-orders/:id} : Partial updates given fields of an existing paymentOrder, field will ignore if it is null
     *
     * @param id the id of the paymentOrderDTO to save.
     * @param paymentOrderDTO the paymentOrderDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated paymentOrderDTO,
     * or with status {@code 400 (Bad Request)} if the paymentOrderDTO is not valid,
     * or with status {@code 404 (Not Found)} if the paymentOrderDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the paymentOrderDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/payment-orders/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<PaymentOrderDTO> partialUpdatePaymentOrder(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PaymentOrderDTO paymentOrderDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update PaymentOrder partially : {}, {}", id, paymentOrderDTO);
        if (paymentOrderDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, paymentOrderDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!paymentOrderRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PaymentOrderDTO> result = paymentOrderService.partialUpdate(paymentOrderDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, paymentOrderDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /payment-orders} : get all the paymentOrders.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of paymentOrders in body.
     */
    @GetMapping("/payment-orders")
    public List<PaymentOrderDTO> getAllPaymentOrders() {
        log.debug("REST request to get all PaymentOrders");
        return paymentOrderService.findAll();
    }

    /**
     * {@code GET  /payment-orders/:id} : get the "id" paymentOrder.
     *
     * @param id the id of the paymentOrderDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the paymentOrderDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/payment-orders/{id}")
    public ResponseEntity<PaymentOrderDTO> getPaymentOrder(@PathVariable Long id) {
        log.debug("REST request to get PaymentOrder : {}", id);
        Optional<PaymentOrderDTO> paymentOrderDTO = paymentOrderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(paymentOrderDTO);
    }

    /**
     * {@code DELETE  /payment-orders/:id} : delete the "id" paymentOrder.
     *
     * @param id the id of the paymentOrderDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/payment-orders/{id}")
    public ResponseEntity<Void> deletePaymentOrder(@PathVariable Long id) {
        log.debug("REST request to delete PaymentOrder : {}", id);
        paymentOrderService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
