package com.uberclone.payment.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A PaymentOrder.
 */
@Entity
@Table(name = "payment_order")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PaymentOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "ride_id", nullable = false, unique = true)
    private String rideId;

    @NotNull
    @Column(name = "amount", nullable = false)
    private Integer amount;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "outgoingPayments", "incomingPayments" }, allowSetters = true)
    private BankAccount fromAccount;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "outgoingPayments", "incomingPayments" }, allowSetters = true)
    private BankAccount toAccount;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PaymentOrder id(Long id) {
        this.id = id;
        return this;
    }

    public String getRideId() {
        return this.rideId;
    }

    public PaymentOrder rideId(String rideId) {
        this.rideId = rideId;
        return this;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public Integer getAmount() {
        return this.amount;
    }

    public PaymentOrder amount(Integer amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public BankAccount getFromAccount() {
        return this.fromAccount;
    }

    public PaymentOrder fromAccount(BankAccount bankAccount) {
        this.setFromAccount(bankAccount);
        return this;
    }

    public void setFromAccount(BankAccount bankAccount) {
        this.fromAccount = bankAccount;
    }

    public BankAccount getToAccount() {
        return this.toAccount;
    }

    public PaymentOrder toAccount(BankAccount bankAccount) {
        this.setToAccount(bankAccount);
        return this;
    }

    public void setToAccount(BankAccount bankAccount) {
        this.toAccount = bankAccount;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PaymentOrder)) {
            return false;
        }
        return id != null && id.equals(((PaymentOrder) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PaymentOrder{" +
            "id=" + getId() +
            ", rideId='" + getRideId() + "'" +
            ", amount=" + getAmount() +
            "}";
    }
}
