package com.uberclone.payment.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A BankAccount.
 */
@Entity
@Table(name = "bank_account")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class BankAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @NotNull
    @Column(name = "amount", nullable = false)
    private Integer amount;

    @OneToMany(mappedBy = "fromAccount")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "fromAccount", "toAccount" }, allowSetters = true)
    private Set<PaymentOrder> outgoingPayments = new HashSet<>();

    @OneToMany(mappedBy = "toAccount")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "fromAccount", "toAccount" }, allowSetters = true)
    private Set<PaymentOrder> incomingPayments = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BankAccount id(Long id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return this.username;
    }

    public BankAccount username(String username) {
        this.username = username;
        return this;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getAmount() {
        return this.amount;
    }

    public BankAccount amount(Integer amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Set<PaymentOrder> getOutgoingPayments() {
        return this.outgoingPayments;
    }

    public BankAccount outgoingPayments(Set<PaymentOrder> paymentOrders) {
        this.setOutgoingPayments(paymentOrders);
        return this;
    }

    public BankAccount addOutgoingPayments(PaymentOrder paymentOrder) {
        this.outgoingPayments.add(paymentOrder);
        paymentOrder.setFromAccount(this);
        return this;
    }

    public BankAccount removeOutgoingPayments(PaymentOrder paymentOrder) {
        this.outgoingPayments.remove(paymentOrder);
        paymentOrder.setFromAccount(null);
        return this;
    }

    public void setOutgoingPayments(Set<PaymentOrder> paymentOrders) {
        if (this.outgoingPayments != null) {
            this.outgoingPayments.forEach(i -> i.setFromAccount(null));
        }
        if (paymentOrders != null) {
            paymentOrders.forEach(i -> i.setFromAccount(this));
        }
        this.outgoingPayments = paymentOrders;
    }

    public Set<PaymentOrder> getIncomingPayments() {
        return this.incomingPayments;
    }

    public BankAccount incomingPayments(Set<PaymentOrder> paymentOrders) {
        this.setIncomingPayments(paymentOrders);
        return this;
    }

    public BankAccount addIncomingPayments(PaymentOrder paymentOrder) {
        this.incomingPayments.add(paymentOrder);
        paymentOrder.setToAccount(this);
        return this;
    }

    public BankAccount removeIncomingPayments(PaymentOrder paymentOrder) {
        this.incomingPayments.remove(paymentOrder);
        paymentOrder.setToAccount(null);
        return this;
    }

    public void setIncomingPayments(Set<PaymentOrder> paymentOrders) {
        if (this.incomingPayments != null) {
            this.incomingPayments.forEach(i -> i.setToAccount(null));
        }
        if (paymentOrders != null) {
            paymentOrders.forEach(i -> i.setToAccount(this));
        }
        this.incomingPayments = paymentOrders;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BankAccount)) {
            return false;
        }
        return id != null && id.equals(((BankAccount) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BankAccount{" +
            "id=" + getId() +
            ", username='" + getUsername() + "'" +
            ", amount=" + getAmount() +
            "}";
    }
}
