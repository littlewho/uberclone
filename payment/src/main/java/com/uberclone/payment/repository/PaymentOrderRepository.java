package com.uberclone.payment.repository;

import com.uberclone.payment.domain.PaymentOrder;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the PaymentOrder entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaymentOrderRepository extends JpaRepository<PaymentOrder, Long> {}
