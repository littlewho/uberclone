package com.uberclone.payment.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.uberclone.payment.domain.PaymentOrder} entity.
 */
public class PaymentOrderDTO implements Serializable {

    private Long id;

    @NotNull
    private String rideId;

    @NotNull
    private Integer amount;

    private BankAccountDTO fromAccount;

    private BankAccountDTO toAccount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public BankAccountDTO getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(BankAccountDTO fromAccount) {
        this.fromAccount = fromAccount;
    }

    public BankAccountDTO getToAccount() {
        return toAccount;
    }

    public void setToAccount(BankAccountDTO toAccount) {
        this.toAccount = toAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PaymentOrderDTO)) {
            return false;
        }

        PaymentOrderDTO paymentOrderDTO = (PaymentOrderDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, paymentOrderDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PaymentOrderDTO{" +
            "id=" + getId() +
            ", rideId='" + getRideId() + "'" +
            ", amount=" + getAmount() +
            ", fromAccount=" + getFromAccount() +
            ", toAccount=" + getToAccount() +
            "}";
    }
}
