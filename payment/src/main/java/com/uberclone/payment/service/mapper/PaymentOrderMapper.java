package com.uberclone.payment.service.mapper;

import com.uberclone.payment.domain.*;
import com.uberclone.payment.service.dto.PaymentOrderDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link PaymentOrder} and its DTO {@link PaymentOrderDTO}.
 */
@Mapper(componentModel = "spring", uses = { BankAccountMapper.class })
public interface PaymentOrderMapper extends EntityMapper<PaymentOrderDTO, PaymentOrder> {
    @Mapping(target = "fromAccount", source = "fromAccount", qualifiedByName = "id")
    @Mapping(target = "toAccount", source = "toAccount", qualifiedByName = "id")
    PaymentOrderDTO toDto(PaymentOrder s);
}
