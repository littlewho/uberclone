package com.uberclone.payment.service;

import com.uberclone.payment.domain.PaymentOrder;
import com.uberclone.payment.repository.PaymentOrderRepository;
import com.uberclone.payment.service.dto.PaymentOrderDTO;
import com.uberclone.payment.service.mapper.PaymentOrderMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PaymentOrder}.
 */
@Service
@Transactional
public class PaymentOrderService {

    private final Logger log = LoggerFactory.getLogger(PaymentOrderService.class);

    private final PaymentOrderRepository paymentOrderRepository;

    private final PaymentOrderMapper paymentOrderMapper;

    public PaymentOrderService(PaymentOrderRepository paymentOrderRepository, PaymentOrderMapper paymentOrderMapper) {
        this.paymentOrderRepository = paymentOrderRepository;
        this.paymentOrderMapper = paymentOrderMapper;
    }

    /**
     * Save a paymentOrder.
     *
     * @param paymentOrderDTO the entity to save.
     * @return the persisted entity.
     */
    public PaymentOrderDTO save(PaymentOrderDTO paymentOrderDTO) {
        log.debug("Request to save PaymentOrder : {}", paymentOrderDTO);
        PaymentOrder paymentOrder = paymentOrderMapper.toEntity(paymentOrderDTO);
        paymentOrder = paymentOrderRepository.save(paymentOrder);
        return paymentOrderMapper.toDto(paymentOrder);
    }

    /**
     * Partially update a paymentOrder.
     *
     * @param paymentOrderDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<PaymentOrderDTO> partialUpdate(PaymentOrderDTO paymentOrderDTO) {
        log.debug("Request to partially update PaymentOrder : {}", paymentOrderDTO);

        return paymentOrderRepository
            .findById(paymentOrderDTO.getId())
            .map(
                existingPaymentOrder -> {
                    paymentOrderMapper.partialUpdate(existingPaymentOrder, paymentOrderDTO);
                    return existingPaymentOrder;
                }
            )
            .map(paymentOrderRepository::save)
            .map(paymentOrderMapper::toDto);
    }

    /**
     * Get all the paymentOrders.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PaymentOrderDTO> findAll() {
        log.debug("Request to get all PaymentOrders");
        return paymentOrderRepository.findAll().stream().map(paymentOrderMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one paymentOrder by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PaymentOrderDTO> findOne(Long id) {
        log.debug("Request to get PaymentOrder : {}", id);
        return paymentOrderRepository.findById(id).map(paymentOrderMapper::toDto);
    }

    /**
     * Delete the paymentOrder by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PaymentOrder : {}", id);
        paymentOrderRepository.deleteById(id);
    }
}
