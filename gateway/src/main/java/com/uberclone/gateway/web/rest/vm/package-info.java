/**
 * View Models used by Spring MVC REST controllers.
 */
package com.uberclone.gateway.web.rest.vm;
