import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import locale, { LocaleState } from './locale';
import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
import userManagement, { UserManagementState } from 'app/modules/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'app/modules/account/register/register.reducer';
import activate, { ActivateState } from 'app/modules/account/activate/activate.reducer';
import password, { PasswordState } from 'app/modules/account/password/password.reducer';
import settings, { SettingsState } from 'app/modules/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'app/modules/account/password-reset/password-reset.reducer';
// prettier-ignore
import rideSummary, {
  RideSummaryState
} from 'app/entities/ride/ride-summary/ride-summary.reducer';
// prettier-ignore
import bankAccount, {
  BankAccountState
} from 'app/entities/payment/bank-account/bank-account.reducer';
// prettier-ignore
import userProfile, {
  UserProfileState
} from 'app/entities/profile/user-profile/user-profile.reducer';
// prettier-ignore
import ride, {
  RideState
} from 'app/entities/ride/ride/ride.reducer';
// prettier-ignore
import car, {
  CarState
} from 'app/entities/profile/car/car.reducer';
// prettier-ignore
import paymentOrder, {
  PaymentOrderState
} from 'app/entities/payment/payment-order/payment-order.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly locale: LocaleState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  readonly register: RegisterState;
  readonly activate: ActivateState;
  readonly passwordReset: PasswordResetState;
  readonly password: PasswordState;
  readonly settings: SettingsState;
  readonly rideSummary: RideSummaryState;
  readonly bankAccount: BankAccountState;
  readonly userProfile: UserProfileState;
  readonly ride: RideState;
  readonly car: CarState;
  readonly paymentOrder: PaymentOrderState;
  /* jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  locale,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  rideSummary,
  bankAccount,
  userProfile,
  ride,
  car,
  paymentOrder,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar,
});

export default rootReducer;
