import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { Translate, translate } from 'react-jhipster';
import { NavDropdown } from './menu-components';

export const EntitiesMenu = props => (
  <NavDropdown
    icon="th-list"
    name={translate('global.menu.entities.main')}
    id="entity-menu"
    data-cy="entity"
    style={{ maxHeight: '80vh', overflow: 'auto' }}
  >
    <MenuItem icon="asterisk" to="/ride-summary">
      <Translate contentKey="global.menu.entities.rideRideSummary" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/bank-account">
      <Translate contentKey="global.menu.entities.paymentBankAccount" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/user-profile">
      <Translate contentKey="global.menu.entities.profileUserProfile" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/ride">
      <Translate contentKey="global.menu.entities.rideRide" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/car">
      <Translate contentKey="global.menu.entities.profileCar" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/payment-order">
      <Translate contentKey="global.menu.entities.paymentPaymentOrder" />
    </MenuItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);
