export interface ICar {
  id?: number;
  username?: string;
  model?: string;
  color?: string;
  comfort?: number;
}

export const defaultValue: Readonly<ICar> = {};
