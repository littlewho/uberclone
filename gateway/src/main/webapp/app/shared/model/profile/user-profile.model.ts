export interface IUserProfile {
  id?: number;
  username?: string;
  isDriver?: boolean;
}

export const defaultValue: Readonly<IUserProfile> = {
  isDriver: false,
};
