import { IPaymentOrder } from 'app/shared/model/payment/payment-order.model';

export interface IBankAccount {
  id?: number;
  username?: string;
  amount?: number;
  outgoingPayments?: IPaymentOrder[] | null;
  incomingPayments?: IPaymentOrder[] | null;
}

export const defaultValue: Readonly<IBankAccount> = {};
