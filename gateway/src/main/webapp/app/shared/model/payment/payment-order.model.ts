import { IBankAccount } from 'app/shared/model/payment/bank-account.model';

export interface IPaymentOrder {
  id?: number;
  rideId?: string;
  amount?: number;
  fromAccount?: IBankAccount;
  toAccount?: IBankAccount;
}

export const defaultValue: Readonly<IPaymentOrder> = {};
