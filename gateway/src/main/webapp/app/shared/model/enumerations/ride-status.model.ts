export enum RideStatus {
  COMPLETED = 'COMPLETED',

  CANCELED = 'CANCELED',

  ABUSE_REPORT = 'ABUSE_REPORT',
}
