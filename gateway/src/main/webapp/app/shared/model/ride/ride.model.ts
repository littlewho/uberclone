export interface IRide {
  id?: number;
  customerUsername?: string;
  driverUsername?: string;
  startLatitude?: number;
  startLongitude?: number;
  endLatitude?: number;
  endLongitude?: number;
  comfort?: number;
  price?: number;
  time?: number;
  kilometers?: number;
}

export const defaultValue: Readonly<IRide> = {};
