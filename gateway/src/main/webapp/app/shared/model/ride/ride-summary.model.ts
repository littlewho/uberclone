import { RideStatus } from 'app/shared/model/enumerations/ride-status.model';

export interface IRideSummary {
  id?: number;
  rideId?: string;
  status?: RideStatus;
  stars?: number;
  comment?: string;
}

export const defaultValue: Readonly<IRideSummary> = {};
