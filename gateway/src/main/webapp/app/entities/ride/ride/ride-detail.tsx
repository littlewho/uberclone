import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './ride.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IRideDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const RideDetail = (props: IRideDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { rideEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="rideDetailsHeading">
          <Translate contentKey="gatewayApp.rideRide.detail.title">Ride</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{rideEntity.id}</dd>
          <dt>
            <span id="customerUsername">
              <Translate contentKey="gatewayApp.rideRide.customerUsername">Customer Username</Translate>
            </span>
          </dt>
          <dd>{rideEntity.customerUsername}</dd>
          <dt>
            <span id="driverUsername">
              <Translate contentKey="gatewayApp.rideRide.driverUsername">Driver Username</Translate>
            </span>
          </dt>
          <dd>{rideEntity.driverUsername}</dd>
          <dt>
            <span id="startLatitude">
              <Translate contentKey="gatewayApp.rideRide.startLatitude">Start Latitude</Translate>
            </span>
          </dt>
          <dd>{rideEntity.startLatitude}</dd>
          <dt>
            <span id="startLongitude">
              <Translate contentKey="gatewayApp.rideRide.startLongitude">Start Longitude</Translate>
            </span>
          </dt>
          <dd>{rideEntity.startLongitude}</dd>
          <dt>
            <span id="endLatitude">
              <Translate contentKey="gatewayApp.rideRide.endLatitude">End Latitude</Translate>
            </span>
          </dt>
          <dd>{rideEntity.endLatitude}</dd>
          <dt>
            <span id="endLongitude">
              <Translate contentKey="gatewayApp.rideRide.endLongitude">End Longitude</Translate>
            </span>
          </dt>
          <dd>{rideEntity.endLongitude}</dd>
          <dt>
            <span id="comfort">
              <Translate contentKey="gatewayApp.rideRide.comfort">Comfort</Translate>
            </span>
          </dt>
          <dd>{rideEntity.comfort}</dd>
          <dt>
            <span id="price">
              <Translate contentKey="gatewayApp.rideRide.price">Price</Translate>
            </span>
          </dt>
          <dd>{rideEntity.price}</dd>
          <dt>
            <span id="time">
              <Translate contentKey="gatewayApp.rideRide.time">Time</Translate>
            </span>
          </dt>
          <dd>{rideEntity.time}</dd>
          <dt>
            <span id="kilometers">
              <Translate contentKey="gatewayApp.rideRide.kilometers">Kilometers</Translate>
            </span>
          </dt>
          <dd>{rideEntity.kilometers}</dd>
        </dl>
        <Button tag={Link} to="/ride" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/ride/${rideEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ ride }: IRootState) => ({
  rideEntity: ride.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(RideDetail);
