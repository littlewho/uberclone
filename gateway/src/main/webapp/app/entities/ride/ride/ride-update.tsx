import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './ride.reducer';
import { IRide } from 'app/shared/model/ride/ride.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IRideUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const RideUpdate = (props: IRideUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { rideEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/ride');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...rideEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.rideRide.home.createOrEditLabel" data-cy="RideCreateUpdateHeading">
            <Translate contentKey="gatewayApp.rideRide.home.createOrEditLabel">Create or edit a Ride</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : rideEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="ride-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="ride-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="customerUsernameLabel" for="ride-customerUsername">
                  <Translate contentKey="gatewayApp.rideRide.customerUsername">Customer Username</Translate>
                </Label>
                <AvField
                  id="ride-customerUsername"
                  data-cy="customerUsername"
                  type="text"
                  name="customerUsername"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="driverUsernameLabel" for="ride-driverUsername">
                  <Translate contentKey="gatewayApp.rideRide.driverUsername">Driver Username</Translate>
                </Label>
                <AvField
                  id="ride-driverUsername"
                  data-cy="driverUsername"
                  type="text"
                  name="driverUsername"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="startLatitudeLabel" for="ride-startLatitude">
                  <Translate contentKey="gatewayApp.rideRide.startLatitude">Start Latitude</Translate>
                </Label>
                <AvField
                  id="ride-startLatitude"
                  data-cy="startLatitude"
                  type="string"
                  className="form-control"
                  name="startLatitude"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="startLongitudeLabel" for="ride-startLongitude">
                  <Translate contentKey="gatewayApp.rideRide.startLongitude">Start Longitude</Translate>
                </Label>
                <AvField
                  id="ride-startLongitude"
                  data-cy="startLongitude"
                  type="string"
                  className="form-control"
                  name="startLongitude"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="endLatitudeLabel" for="ride-endLatitude">
                  <Translate contentKey="gatewayApp.rideRide.endLatitude">End Latitude</Translate>
                </Label>
                <AvField
                  id="ride-endLatitude"
                  data-cy="endLatitude"
                  type="string"
                  className="form-control"
                  name="endLatitude"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="endLongitudeLabel" for="ride-endLongitude">
                  <Translate contentKey="gatewayApp.rideRide.endLongitude">End Longitude</Translate>
                </Label>
                <AvField
                  id="ride-endLongitude"
                  data-cy="endLongitude"
                  type="string"
                  className="form-control"
                  name="endLongitude"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="comfortLabel" for="ride-comfort">
                  <Translate contentKey="gatewayApp.rideRide.comfort">Comfort</Translate>
                </Label>
                <AvField
                  id="ride-comfort"
                  data-cy="comfort"
                  type="string"
                  className="form-control"
                  name="comfort"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="priceLabel" for="ride-price">
                  <Translate contentKey="gatewayApp.rideRide.price">Price</Translate>
                </Label>
                <AvField
                  id="ride-price"
                  data-cy="price"
                  type="string"
                  className="form-control"
                  name="price"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="timeLabel" for="ride-time">
                  <Translate contentKey="gatewayApp.rideRide.time">Time</Translate>
                </Label>
                <AvField
                  id="ride-time"
                  data-cy="time"
                  type="string"
                  className="form-control"
                  name="time"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="kilometersLabel" for="ride-kilometers">
                  <Translate contentKey="gatewayApp.rideRide.kilometers">Kilometers</Translate>
                </Label>
                <AvField
                  id="ride-kilometers"
                  data-cy="kilometers"
                  type="string"
                  className="form-control"
                  name="kilometers"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/ride" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  rideEntity: storeState.ride.entity,
  loading: storeState.ride.loading,
  updating: storeState.ride.updating,
  updateSuccess: storeState.ride.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(RideUpdate);
