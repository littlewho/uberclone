import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IRide, defaultValue } from 'app/shared/model/ride/ride.model';

export const ACTION_TYPES = {
  FETCH_RIDE_LIST: 'ride/FETCH_RIDE_LIST',
  FETCH_RIDE: 'ride/FETCH_RIDE',
  CREATE_RIDE: 'ride/CREATE_RIDE',
  UPDATE_RIDE: 'ride/UPDATE_RIDE',
  PARTIAL_UPDATE_RIDE: 'ride/PARTIAL_UPDATE_RIDE',
  DELETE_RIDE: 'ride/DELETE_RIDE',
  RESET: 'ride/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IRide>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type RideState = Readonly<typeof initialState>;

// Reducer

export default (state: RideState = initialState, action): RideState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_RIDE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_RIDE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_RIDE):
    case REQUEST(ACTION_TYPES.UPDATE_RIDE):
    case REQUEST(ACTION_TYPES.DELETE_RIDE):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_RIDE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_RIDE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_RIDE):
    case FAILURE(ACTION_TYPES.CREATE_RIDE):
    case FAILURE(ACTION_TYPES.UPDATE_RIDE):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_RIDE):
    case FAILURE(ACTION_TYPES.DELETE_RIDE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_RIDE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_RIDE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_RIDE):
    case SUCCESS(ACTION_TYPES.UPDATE_RIDE):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_RIDE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_RIDE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/ride/api/rides';

// Actions

export const getEntities: ICrudGetAllAction<IRide> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_RIDE_LIST,
  payload: axios.get<IRide>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IRide> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_RIDE,
    payload: axios.get<IRide>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IRide> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_RIDE,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IRide> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_RIDE,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IRide> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_RIDE,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IRide> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_RIDE,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
