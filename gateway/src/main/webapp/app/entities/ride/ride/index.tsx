import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Ride from './ride';
import RideDetail from './ride-detail';
import RideUpdate from './ride-update';
import RideDeleteDialog from './ride-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={RideUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={RideDetail} />
      <ErrorBoundaryRoute path={match.url} component={Ride} />
    </Switch>
  </>
);

export default Routes;
