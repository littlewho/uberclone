import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './ride.reducer';
import { IRide } from 'app/shared/model/ride/ride.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IRideProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Ride = (props: IRideProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const handleSyncList = () => {
    props.getEntities();
  };

  const { rideList, match, loading } = props;
  return (
    <div>
      <h2 id="ride-heading" data-cy="RideHeading">
        <Translate contentKey="gatewayApp.rideRide.home.title">Rides</Translate>
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="gatewayApp.rideRide.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="gatewayApp.rideRide.home.createLabel">Create new Ride</Translate>
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {rideList && rideList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="gatewayApp.rideRide.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.rideRide.customerUsername">Customer Username</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.rideRide.driverUsername">Driver Username</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.rideRide.startLatitude">Start Latitude</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.rideRide.startLongitude">Start Longitude</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.rideRide.endLatitude">End Latitude</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.rideRide.endLongitude">End Longitude</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.rideRide.comfort">Comfort</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.rideRide.price">Price</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.rideRide.time">Time</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.rideRide.kilometers">Kilometers</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {rideList.map((ride, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${ride.id}`} color="link" size="sm">
                      {ride.id}
                    </Button>
                  </td>
                  <td>{ride.customerUsername}</td>
                  <td>{ride.driverUsername}</td>
                  <td>{ride.startLatitude}</td>
                  <td>{ride.startLongitude}</td>
                  <td>{ride.endLatitude}</td>
                  <td>{ride.endLongitude}</td>
                  <td>{ride.comfort}</td>
                  <td>{ride.price}</td>
                  <td>{ride.time}</td>
                  <td>{ride.kilometers}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${ride.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="gatewayApp.rideRide.home.notFound">No Rides found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ ride }: IRootState) => ({
  rideList: ride.entities,
  loading: ride.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Ride);
