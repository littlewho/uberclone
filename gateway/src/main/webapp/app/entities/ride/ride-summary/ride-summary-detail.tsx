import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './ride-summary.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IRideSummaryDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const RideSummaryDetail = (props: IRideSummaryDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { rideSummaryEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="rideSummaryDetailsHeading">
          <Translate contentKey="gatewayApp.rideRideSummary.detail.title">RideSummary</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{rideSummaryEntity.id}</dd>
          <dt>
            <span id="rideId">
              <Translate contentKey="gatewayApp.rideRideSummary.rideId">Ride Id</Translate>
            </span>
          </dt>
          <dd>{rideSummaryEntity.rideId}</dd>
          <dt>
            <span id="status">
              <Translate contentKey="gatewayApp.rideRideSummary.status">Status</Translate>
            </span>
          </dt>
          <dd>{rideSummaryEntity.status}</dd>
          <dt>
            <span id="stars">
              <Translate contentKey="gatewayApp.rideRideSummary.stars">Stars</Translate>
            </span>
          </dt>
          <dd>{rideSummaryEntity.stars}</dd>
          <dt>
            <span id="comment">
              <Translate contentKey="gatewayApp.rideRideSummary.comment">Comment</Translate>
            </span>
          </dt>
          <dd>{rideSummaryEntity.comment}</dd>
        </dl>
        <Button tag={Link} to="/ride-summary" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/ride-summary/${rideSummaryEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ rideSummary }: IRootState) => ({
  rideSummaryEntity: rideSummary.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(RideSummaryDetail);
