import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './ride-summary.reducer';
import { IRideSummary } from 'app/shared/model/ride/ride-summary.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IRideSummaryUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const RideSummaryUpdate = (props: IRideSummaryUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { rideSummaryEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/ride-summary');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...rideSummaryEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.rideRideSummary.home.createOrEditLabel" data-cy="RideSummaryCreateUpdateHeading">
            <Translate contentKey="gatewayApp.rideRideSummary.home.createOrEditLabel">Create or edit a RideSummary</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : rideSummaryEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <>
                  <AvGroup>
                    <Label for="ride-summary-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="ride-summary-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>

                  <AvGroup>
                    <Label for="ride-summary-rideId">
                      <Translate contentKey="gatewayApp.rideRideSummary.rideId">Ride ID</Translate>
                    </Label>
                    <AvInput id="ride-summary-rideId" type="text" className="form-control" name="rideId" required readOnly />
                  </AvGroup>
                </>
              ) : (
                <>
                  <AvGroup>
                    <Label id="rideIdLabel" for="ride-summary-rideId">
                      <Translate contentKey="gatewayApp.rideRideSummary.rideId">Ride Id</Translate>
                    </Label>
                    <AvField
                      id="ride-summary-rideId"
                      data-cy="rideId"
                      type="text"
                      name="rideId"
                      validate={{
                        required: { value: true, errorMessage: translate('entity.validation.required') },
                      }}
                    />
                  </AvGroup>
                </>
              )}
              <AvGroup>
                <Label id="statusLabel" for="ride-summary-status">
                  <Translate contentKey="gatewayApp.rideRideSummary.status">Status</Translate>
                </Label>
                <AvInput
                  id="ride-summary-status"
                  data-cy="status"
                  type="select"
                  className="form-control"
                  name="status"
                  value={(!isNew && rideSummaryEntity.status) || 'COMPLETED'}
                >
                  <option value="COMPLETED">{translate('gatewayApp.RideStatus.COMPLETED')}</option>
                  <option value="CANCELED">{translate('gatewayApp.RideStatus.CANCELED')}</option>
                  <option value="ABUSE_REPORT">{translate('gatewayApp.RideStatus.ABUSE_REPORT')}</option>
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label id="starsLabel" for="ride-summary-stars">
                  <Translate contentKey="gatewayApp.rideRideSummary.stars">Stars</Translate>
                </Label>
                <AvField
                  id="ride-summary-stars"
                  data-cy="stars"
                  type="string"
                  className="form-control"
                  name="stars"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="commentLabel" for="ride-summary-comment">
                  <Translate contentKey="gatewayApp.rideRideSummary.comment">Comment</Translate>
                </Label>
                <AvField
                  id="ride-summary-comment"
                  data-cy="comment"
                  type="text"
                  name="comment"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/ride-summary" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  rideSummaryEntity: storeState.rideSummary.entity,
  loading: storeState.rideSummary.loading,
  updating: storeState.rideSummary.updating,
  updateSuccess: storeState.rideSummary.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(RideSummaryUpdate);
