import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IRideSummary, defaultValue } from 'app/shared/model/ride/ride-summary.model';

export const ACTION_TYPES = {
  FETCH_RIDESUMMARY_LIST: 'rideSummary/FETCH_RIDESUMMARY_LIST',
  FETCH_RIDESUMMARY: 'rideSummary/FETCH_RIDESUMMARY',
  CREATE_RIDESUMMARY: 'rideSummary/CREATE_RIDESUMMARY',
  UPDATE_RIDESUMMARY: 'rideSummary/UPDATE_RIDESUMMARY',
  PARTIAL_UPDATE_RIDESUMMARY: 'rideSummary/PARTIAL_UPDATE_RIDESUMMARY',
  DELETE_RIDESUMMARY: 'rideSummary/DELETE_RIDESUMMARY',
  RESET: 'rideSummary/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IRideSummary>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type RideSummaryState = Readonly<typeof initialState>;

// Reducer

export default (state: RideSummaryState = initialState, action): RideSummaryState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_RIDESUMMARY_LIST):
    case REQUEST(ACTION_TYPES.FETCH_RIDESUMMARY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_RIDESUMMARY):
    case REQUEST(ACTION_TYPES.UPDATE_RIDESUMMARY):
    case REQUEST(ACTION_TYPES.DELETE_RIDESUMMARY):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_RIDESUMMARY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_RIDESUMMARY_LIST):
    case FAILURE(ACTION_TYPES.FETCH_RIDESUMMARY):
    case FAILURE(ACTION_TYPES.CREATE_RIDESUMMARY):
    case FAILURE(ACTION_TYPES.UPDATE_RIDESUMMARY):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_RIDESUMMARY):
    case FAILURE(ACTION_TYPES.DELETE_RIDESUMMARY):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_RIDESUMMARY_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_RIDESUMMARY):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_RIDESUMMARY):
    case SUCCESS(ACTION_TYPES.UPDATE_RIDESUMMARY):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_RIDESUMMARY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_RIDESUMMARY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/ride/api/ride-summaries';

// Actions

export const getEntities: ICrudGetAllAction<IRideSummary> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_RIDESUMMARY_LIST,
  payload: axios.get<IRideSummary>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IRideSummary> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_RIDESUMMARY,
    payload: axios.get<IRideSummary>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IRideSummary> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_RIDESUMMARY,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IRideSummary> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_RIDESUMMARY,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IRideSummary> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_RIDESUMMARY,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IRideSummary> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_RIDESUMMARY,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
