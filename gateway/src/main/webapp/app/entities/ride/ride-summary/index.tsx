import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import RideSummary from './ride-summary';
import RideSummaryDetail from './ride-summary-detail';
import RideSummaryUpdate from './ride-summary-update';
import RideSummaryDeleteDialog from './ride-summary-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={RideSummaryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={RideSummaryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={RideSummaryDetail} />
      <ErrorBoundaryRoute path={match.url} component={RideSummary} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={RideSummaryDeleteDialog} />
  </>
);

export default Routes;
