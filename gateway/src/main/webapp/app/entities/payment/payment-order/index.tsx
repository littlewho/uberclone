import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import PaymentOrder from './payment-order';
import PaymentOrderDetail from './payment-order-detail';
import PaymentOrderUpdate from './payment-order-update';
import PaymentOrderDeleteDialog from './payment-order-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PaymentOrderUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PaymentOrderUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PaymentOrderDetail} />
      <ErrorBoundaryRoute path={match.url} component={PaymentOrder} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={PaymentOrderDeleteDialog} />
  </>
);

export default Routes;
