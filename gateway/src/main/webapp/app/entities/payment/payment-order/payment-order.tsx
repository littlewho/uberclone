import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './payment-order.reducer';
import { IPaymentOrder } from 'app/shared/model/payment/payment-order.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPaymentOrderProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const PaymentOrder = (props: IPaymentOrderProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const handleSyncList = () => {
    props.getEntities();
  };

  const { paymentOrderList, match, loading } = props;
  return (
    <div>
      <h2 id="payment-order-heading" data-cy="PaymentOrderHeading">
        <Translate contentKey="gatewayApp.paymentPaymentOrder.home.title">Payment Orders</Translate>
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="gatewayApp.paymentPaymentOrder.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="gatewayApp.paymentPaymentOrder.home.createLabel">Create new Payment Order</Translate>
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {paymentOrderList && paymentOrderList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="gatewayApp.paymentPaymentOrder.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.paymentPaymentOrder.rideId">Ride Id</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.paymentPaymentOrder.amount">Amount</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.paymentPaymentOrder.fromAccount">From Account</Translate>
                </th>
                <th>
                  <Translate contentKey="gatewayApp.paymentPaymentOrder.toAccount">To Account</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {paymentOrderList.map((paymentOrder, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${paymentOrder.id}`} color="link" size="sm">
                      {paymentOrder.id}
                    </Button>
                  </td>
                  <td>{paymentOrder.id}</td>
                  <td>{paymentOrder.rideId}</td>
                  <td>{paymentOrder.amount}</td>
                  <td>
                    {paymentOrder.fromAccount ? (
                      <Link to={`bank-account/${paymentOrder.fromAccount.id}`}>{paymentOrder.fromAccount.id}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td>
                    {paymentOrder.toAccount ? (
                      <Link to={`bank-account/${paymentOrder.toAccount.id}`}>{paymentOrder.toAccount.id}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${paymentOrder.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${paymentOrder.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${paymentOrder.id}/delete`}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="gatewayApp.paymentPaymentOrder.home.notFound">No Payment Orders found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ paymentOrder }: IRootState) => ({
  paymentOrderList: paymentOrder.entities,
  loading: paymentOrder.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PaymentOrder);
