import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IPaymentOrder, defaultValue } from 'app/shared/model/payment/payment-order.model';

export const ACTION_TYPES = {
  FETCH_PAYMENTORDER_LIST: 'paymentOrder/FETCH_PAYMENTORDER_LIST',
  FETCH_PAYMENTORDER: 'paymentOrder/FETCH_PAYMENTORDER',
  CREATE_PAYMENTORDER: 'paymentOrder/CREATE_PAYMENTORDER',
  UPDATE_PAYMENTORDER: 'paymentOrder/UPDATE_PAYMENTORDER',
  PARTIAL_UPDATE_PAYMENTORDER: 'paymentOrder/PARTIAL_UPDATE_PAYMENTORDER',
  DELETE_PAYMENTORDER: 'paymentOrder/DELETE_PAYMENTORDER',
  RESET: 'paymentOrder/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPaymentOrder>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type PaymentOrderState = Readonly<typeof initialState>;

// Reducer

export default (state: PaymentOrderState = initialState, action): PaymentOrderState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PAYMENTORDER_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PAYMENTORDER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_PAYMENTORDER):
    case REQUEST(ACTION_TYPES.UPDATE_PAYMENTORDER):
    case REQUEST(ACTION_TYPES.DELETE_PAYMENTORDER):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_PAYMENTORDER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_PAYMENTORDER_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PAYMENTORDER):
    case FAILURE(ACTION_TYPES.CREATE_PAYMENTORDER):
    case FAILURE(ACTION_TYPES.UPDATE_PAYMENTORDER):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_PAYMENTORDER):
    case FAILURE(ACTION_TYPES.DELETE_PAYMENTORDER):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PAYMENTORDER_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PAYMENTORDER):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_PAYMENTORDER):
    case SUCCESS(ACTION_TYPES.UPDATE_PAYMENTORDER):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_PAYMENTORDER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_PAYMENTORDER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/payment/api/payment-orders';

// Actions

export const getEntities: ICrudGetAllAction<IPaymentOrder> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_PAYMENTORDER_LIST,
  payload: axios.get<IPaymentOrder>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IPaymentOrder> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PAYMENTORDER,
    payload: axios.get<IPaymentOrder>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IPaymentOrder> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PAYMENTORDER,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IPaymentOrder> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PAYMENTORDER,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IPaymentOrder> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_PAYMENTORDER,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IPaymentOrder> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PAYMENTORDER,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
