import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IBankAccount } from 'app/shared/model/payment/bank-account.model';
import { getEntities as getBankAccounts } from 'app/entities/payment/bank-account/bank-account.reducer';
import { getEntity, updateEntity, createEntity, reset } from './payment-order.reducer';
import { IPaymentOrder } from 'app/shared/model/payment/payment-order.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPaymentOrderUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PaymentOrderUpdate = (props: IPaymentOrderUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { paymentOrderEntity, bankAccounts, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/payment-order');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getBankAccounts();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...paymentOrderEntity,
        ...values,
        fromAccount: bankAccounts.find(it => it.id.toString() === values.fromAccountId.toString()),
        toAccount: bankAccounts.find(it => it.id.toString() === values.toAccountId.toString()),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.paymentPaymentOrder.home.createOrEditLabel" data-cy="PaymentOrderCreateUpdateHeading">
            <Translate contentKey="gatewayApp.paymentPaymentOrder.home.createOrEditLabel">Create or edit a PaymentOrder</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : paymentOrderEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="payment-order-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="payment-order-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="rideIdLabel" for="payment-order-rideId">
                  <Translate contentKey="gatewayApp.paymentPaymentOrder.rideId">Ride Id</Translate>
                </Label>
                <AvField
                  id="payment-order-rideId"
                  data-cy="rideId"
                  type="text"
                  name="rideId"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="amountLabel" for="payment-order-amount">
                  <Translate contentKey="gatewayApp.paymentPaymentOrder.amount">Amount</Translate>
                </Label>
                <AvField
                  id="payment-order-amount"
                  data-cy="amount"
                  type="string"
                  className="form-control"
                  name="amount"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label for="payment-order-fromAccount">
                  <Translate contentKey="gatewayApp.paymentPaymentOrder.fromAccount">From Account</Translate>
                </Label>
                <AvInput
                  id="payment-order-fromAccount"
                  data-cy="fromAccount"
                  type="select"
                  className="form-control"
                  name="fromAccountId"
                  required
                >
                  <option value="" key="0" />
                  {bankAccounts
                    ? bankAccounts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="payment-order-toAccount">
                  <Translate contentKey="gatewayApp.paymentPaymentOrder.toAccount">To Account</Translate>
                </Label>
                <AvInput
                  id="payment-order-toAccount"
                  data-cy="toAccount"
                  type="select"
                  className="form-control"
                  name="toAccountId"
                  required
                >
                  <option value="" key="0" />
                  {bankAccounts
                    ? bankAccounts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/payment-order" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  bankAccounts: storeState.bankAccount.entities,
  paymentOrderEntity: storeState.paymentOrder.entity,
  loading: storeState.paymentOrder.loading,
  updating: storeState.paymentOrder.updating,
  updateSuccess: storeState.paymentOrder.updateSuccess,
});

const mapDispatchToProps = {
  getBankAccounts,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PaymentOrderUpdate);
