import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './payment-order.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPaymentOrderDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PaymentOrderDetail = (props: IPaymentOrderDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { paymentOrderEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="paymentOrderDetailsHeading">
          <Translate contentKey="gatewayApp.paymentPaymentOrder.detail.title">PaymentOrder</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{paymentOrderEntity.id}</dd>
          <dt>
            <span id="rideId">
              <Translate contentKey="gatewayApp.paymentPaymentOrder.rideId">Ride Id</Translate>
            </span>
          </dt>
          <dd>{paymentOrderEntity.rideId}</dd>
          <dt>
            <span id="amount">
              <Translate contentKey="gatewayApp.paymentPaymentOrder.amount">Amount</Translate>
            </span>
          </dt>
          <dd>{paymentOrderEntity.amount}</dd>
          <dt>
            <Translate contentKey="gatewayApp.paymentPaymentOrder.fromAccount">From Account</Translate>
          </dt>
          <dd>{paymentOrderEntity.fromAccount ? paymentOrderEntity.fromAccount.id : ''}</dd>
          <dt>
            <Translate contentKey="gatewayApp.paymentPaymentOrder.toAccount">To Account</Translate>
          </dt>
          <dd>{paymentOrderEntity.toAccount ? paymentOrderEntity.toAccount.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/payment-order" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/payment-order/${paymentOrderEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ paymentOrder }: IRootState) => ({
  paymentOrderEntity: paymentOrder.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PaymentOrderDetail);
