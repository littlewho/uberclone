import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './car.reducer';
import { ICar } from 'app/shared/model/profile/car.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ICarUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const CarUpdate = (props: ICarUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { carEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/car');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...carEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity({ username: 'will be added', ...entity });
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="gatewayApp.profileCar.home.createOrEditLabel" data-cy="CarCreateUpdateHeading">
            <Translate contentKey="gatewayApp.profileCar.home.createOrEditLabel">Create or edit a Car</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : carEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <>
                  <AvGroup>
                    <Label for="car-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="car-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>

                  <AvGroup>
                    <Label id="usernameLabel" for="car-username">
                      <Translate contentKey="gatewayApp.profileCar.username">Username</Translate>
                    </Label>
                    <AvField
                      id="car-username"
                      data-cy="username"
                      type="text"
                      name="username"
                      validate={{
                        required: { value: true, errorMessage: translate('entity.validation.required') },
                      }}
                    />
                  </AvGroup>
                </>
              ) : null}
              <AvGroup>
                <Label id="modelLabel" for="car-model">
                  <Translate contentKey="gatewayApp.profileCar.model">Model</Translate>
                </Label>
                <AvField
                  id="car-model"
                  data-cy="model"
                  type="text"
                  name="model"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="colorLabel" for="car-color">
                  <Translate contentKey="gatewayApp.profileCar.color">Color</Translate>
                </Label>
                <AvField
                  id="car-color"
                  data-cy="color"
                  type="text"
                  name="color"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="comfortLabel" for="car-comfort">
                  <Translate contentKey="gatewayApp.profileCar.comfort">Comfort</Translate>
                </Label>
                <AvField
                  id="car-comfort"
                  data-cy="comfort"
                  type="string"
                  className="form-control"
                  name="comfort"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/car" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  carEntity: storeState.car.entity,
  loading: storeState.car.loading,
  updating: storeState.car.updating,
  updateSuccess: storeState.car.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(CarUpdate);
