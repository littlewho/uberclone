package com.uberclone.profile.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

public class CarDTO implements Serializable {

    private String id;

    @NotNull
    private String username;

    @NotNull
    private String model;

    @NotNull
    private String color;

    @NotNull
    private Integer comfort;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getComfort() {
        return comfort;
    }

    public void setComfort(Integer comfort) {
        this.comfort = comfort;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CarDTO)) {
            return false;
        }

        CarDTO carDTO = (CarDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, carDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CarDTO{" +
            "id='" + getId() + "'" +
            ", username='" + getUsername() + "'" +
            ", model='" + getModel() + "'" +
            ", color='" + getColor() + "'" +
            ", comfort=" + getComfort() +
            "}";
    }
}
