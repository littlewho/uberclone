package com.uberclone.profile.service;

import com.uberclone.profile.domain.Car;
import com.uberclone.profile.repository.CarRepository;
import com.uberclone.profile.service.dto.CarDTO;
import com.uberclone.profile.service.mapper.CarMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CarService {
    private final Logger log = LoggerFactory.getLogger(CarService.class);
    private final CarRepository carRepository;
    private final CarMapper carMapper;

    public CarService(CarRepository carRepository, CarMapper carMapper) {
        this.carRepository = carRepository;
        this.carMapper = carMapper;
    }

    public CarDTO save(CarDTO carDTO) {
        log.debug("Request to save Car : {}", carDTO);
        Car car = carMapper.toEntity(carDTO);
        car = carRepository.save(car);
        return carMapper.toDto(car);
    }

    public Optional<CarDTO> partialUpdate(CarDTO carDTO) {
        log.debug("Request to partially update Car : {}", carDTO);

        return carRepository
            .findById(carDTO.getId())
            .map(
                existingCar -> {
                    carMapper.partialUpdate(existingCar, carDTO);
                    return existingCar;
                }
            )
            .map(carRepository::save)
            .map(carMapper::toDto);
    }

    public List<CarDTO> findAll() {
        log.debug("Request to get all Cars");
        return carRepository.findAll().stream().map(carMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    public List<CarDTO> findAllByUsername(String username) {
        return carRepository.findAllByUsername(username).stream().map(carMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    public Optional<CarDTO> findOne(String id) {
        log.debug("Request to get Car : {}", id);
        return carRepository.findById(id).map(carMapper::toDto);
    }

    public void delete(String id) {
        log.debug("Request to delete Car : {}", id);
        carRepository.deleteById(id);
    }
}
