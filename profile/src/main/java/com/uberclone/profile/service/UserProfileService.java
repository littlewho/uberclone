package com.uberclone.profile.service;

import com.uberclone.profile.domain.UserProfile;
import com.uberclone.profile.repository.UserProfileRepository;
import com.uberclone.profile.service.dto.UserProfileDTO;
import com.uberclone.profile.service.mapper.UserProfileMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class UserProfileService {
    private final Logger log = LoggerFactory.getLogger(UserProfileService.class);
    private final UserProfileRepository userProfileRepository;
    private final UserProfileMapper userProfileMapper;

    public UserProfileService(UserProfileRepository userProfileRepository, UserProfileMapper userProfileMapper) {
        this.userProfileRepository = userProfileRepository;
        this.userProfileMapper = userProfileMapper;
    }

    public UserProfileDTO save(UserProfileDTO userProfileDTO) {
        log.debug("Request to save UserProfile : {}", userProfileDTO);
        UserProfile userProfile = userProfileMapper.toEntity(userProfileDTO);
        userProfile = userProfileRepository.save(userProfile);
        return userProfileMapper.toDto(userProfile);
    }

    public Optional<UserProfileDTO> partialUpdate(UserProfileDTO userProfileDTO) {
        log.debug("Request to partially update UserProfile : {}", userProfileDTO);

        return userProfileRepository
            .findById(userProfileDTO.getId())
            .map(
                existingUserProfile -> {
                    userProfileMapper.partialUpdate(existingUserProfile, userProfileDTO);
                    return existingUserProfile;
                }
            )
            .map(userProfileRepository::save)
            .map(userProfileMapper::toDto);
    }

    public List<UserProfileDTO> findAll() {
        log.debug("Request to get all UserProfiles");
        return userProfileRepository.findAll().stream().map(userProfileMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    public boolean isAlreadyExisting(String username) {
        var alreadyExisting = userProfileRepository.findByUsername(username);
        return alreadyExisting.isPresent();
    }

    public Optional<UserProfileDTO> findOneByUsername(String username) {
        return userProfileRepository.findByUsername(username).map(userProfileMapper::toDto);
    }
}
