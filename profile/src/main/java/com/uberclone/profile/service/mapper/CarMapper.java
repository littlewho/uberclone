package com.uberclone.profile.service.mapper;

import com.uberclone.profile.domain.*;
import com.uberclone.profile.service.dto.CarDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Car} and its DTO {@link CarDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CarMapper extends EntityMapper<CarDTO, Car> {}
