package com.uberclone.profile.repository;

import com.uberclone.profile.domain.Car;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@SuppressWarnings("unused")
@Repository
public interface CarRepository extends MongoRepository<Car, String> {
    public List<Car> findAllByUsername(String username);
}
