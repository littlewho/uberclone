package com.uberclone.profile.domain;

import java.io.Serializable;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A Car.
 */
@Document(collection = "car")
public class Car implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("username")
    private String username;

    @NotNull
    @Field("model")
    private String model;

    @NotNull
    @Field("color")
    private String color;

    @NotNull
    @Field("comfort")
    private Integer comfort;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Car id(String id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return this.username;
    }

    public Car username(String username) {
        this.username = username;
        return this;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getModel() {
        return this.model;
    }

    public Car model(String model) {
        this.model = model;
        return this;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return this.color;
    }

    public Car color(String color) {
        this.color = color;
        return this;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getComfort() {
        return this.comfort;
    }

    public Car comfort(Integer comfort) {
        this.comfort = comfort;
        return this;
    }

    public void setComfort(Integer comfort) {
        this.comfort = comfort;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Car)) {
            return false;
        }
        return id != null && id.equals(((Car) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Car{" +
            "id=" + getId() +
            ", username='" + getUsername() + "'" +
            ", model='" + getModel() + "'" +
            ", color='" + getColor() + "'" +
            ", comfort=" + getComfort() +
            "}";
    }
}
