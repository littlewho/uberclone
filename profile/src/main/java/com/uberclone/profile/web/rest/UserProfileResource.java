package com.uberclone.profile.web.rest;

import com.uberclone.profile.service.UserProfileService;
import com.uberclone.profile.service.dto.UserProfileDTO;
import com.uberclone.profile.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api")
public class UserProfileResource {
    private final Logger log = LoggerFactory.getLogger(UserProfileResource.class);
    private static final String ENTITY_NAME = "profileUserProfile";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserProfileService userProfileService;

    public UserProfileResource(UserProfileService userProfileService) {
        this.userProfileService = userProfileService;
    }

    @PostMapping("/user-profiles")
    public ResponseEntity<UserProfileDTO> createUserProfile(
        Principal principal,
        @Valid @RequestBody UserProfileDTO userProfileDTO) throws URISyntaxException {
        log.debug("REST request to save UserProfile : {}", userProfileDTO);

        userProfileDTO.setId(null);
        userProfileDTO.setUsername(principal.getName());

        if (userProfileService.isAlreadyExisting(userProfileDTO.getUsername())) {
            throw new BadRequestAlertException("This user already has a profile", ENTITY_NAME, "onlyone");
        }

        UserProfileDTO result = userProfileService.save(userProfileDTO);
        return ResponseEntity
            .created(new URI("/api/user-profiles/" + result.getUsername()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getUsername()))
            .body(result);
    }

    @PutMapping("/me")
    public ResponseEntity<UserProfileDTO> updateUserProfileMe (
        Principal principal,
        @Valid @RequestBody UserProfileDTO userProfileDTO
    ) {
        return updateUserProfile(principal, principal.getName(), userProfileDTO);
    }

    @PutMapping("/user-profiles/{username}")
    public ResponseEntity<UserProfileDTO> updateUserProfile(
        Principal principal,
        @PathVariable(value = "username", required = false) final String username,
        @Valid @RequestBody UserProfileDTO userProfileDTO
    ) {
        log.debug("REST request to update UserProfile : {}, {}", username, userProfileDTO);
        validateForUpdate(principal, username, userProfileDTO);

        UserProfileDTO result = userProfileService.save(userProfileDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userProfileDTO.getUsername()))
            .body(result);
    }

    @PatchMapping(value = "/me", consumes = "application/merge-patch+json")
    public ResponseEntity<UserProfileDTO> partialUpdateUserProfileMe(
        Principal principal,
        @NotNull @RequestBody UserProfileDTO userProfileDTO
    ) {
        return partialUpdateUserProfile(principal, principal.getName(), userProfileDTO);
    }

    @PatchMapping(value = "/user-profiles/{username}", consumes = "application/merge-patch+json")
    public ResponseEntity<UserProfileDTO> partialUpdateUserProfile(
        Principal principal,
        @PathVariable(value = "username", required = false) final String username,
        @NotNull @RequestBody UserProfileDTO userProfileDTO
    ) {
        log.debug("REST request to partial update UserProfile partially : {}, {}", username, userProfileDTO);
        validateForUpdate(principal, username, userProfileDTO);

        Optional<UserProfileDTO> result = userProfileService.partialUpdate(userProfileDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userProfileDTO.getUsername())
        );
    }

    @GetMapping("/user-profiles")
    public List<UserProfileDTO> getAllUserProfiles() {
        log.debug("REST request to get all UserProfiles");
        return userProfileService.findAll();
    }

    @GetMapping("/me")
    public ResponseEntity<UserProfileDTO> getUserProfile(Principal principal) {
        return getUserProfile(principal.getName());
    }

    @GetMapping("/user-profiles/{username}")
    public ResponseEntity<UserProfileDTO> getUserProfile(@PathVariable String username) {
        log.debug("REST request to get UserProfile : {}", username);
        Optional<UserProfileDTO> userProfileDTO = userProfileService.findOneByUsername(username);
        return ResponseUtil.wrapOrNotFound(userProfileDTO);
    }

    private void validateForUpdate(Principal principal, String username, UserProfileDTO userProfileDTO) {
        if (userProfileDTO.getUsername() == null) {
            throw new BadRequestAlertException("Invalid username", ENTITY_NAME, "usernamenull");
        }
        if (!Objects.equals(username, userProfileDTO.getUsername())) {
            throw new BadRequestAlertException("Invalid username", ENTITY_NAME, "usernameinvalid");
        }

        var userProfile = userProfileService.findOneByUsername(username);
        if (userProfile.isEmpty()) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "usernamenotfound");
        } else if (!userProfile.get().getUsername().equals(principal.getName())) {
            throw new BadRequestAlertException("You don't own this entity", ENTITY_NAME, "forbidden");
        }

        userProfileDTO.setId(userProfile.get().getId());
    }
}
