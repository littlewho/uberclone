package com.uberclone.profile.web.rest;

import com.uberclone.profile.repository.CarRepository;
import com.uberclone.profile.service.CarService;
import com.uberclone.profile.service.dto.CarDTO;
import com.uberclone.profile.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api")
public class CarResource {
    private final Logger log = LoggerFactory.getLogger(CarResource.class);
    private static final String ENTITY_NAME = "profileCar";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CarService carService;

    public CarResource(CarService carService) {
        this.carService = carService;
    }

    @PostMapping("/cars")
    public ResponseEntity<CarDTO> createCar(Principal principal, @Valid @RequestBody CarDTO carDTO) throws URISyntaxException {
        log.debug("REST request to save Car : {}", carDTO);
        if (carDTO.getId() != null) {
            throw new BadRequestAlertException("A new car cannot already have an ID", ENTITY_NAME, "idexists");
        }

        carDTO.setUsername(principal.getName());

        CarDTO result = carService.save(carDTO);
        return ResponseEntity
            .created(new URI("/api/cars/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @PutMapping("/cars/{id}")
    public ResponseEntity<CarDTO> updateCar(
        Principal principal,
        @PathVariable(value = "id", required = false) final String id,
        @Valid @RequestBody CarDTO carDTO
    ) {
        log.debug("REST request to update Car : {}, {}", id, carDTO);
        validateForUpdate(principal, id, carDTO);

        CarDTO result = carService.save(carDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, carDTO.getId()))
            .body(result);
    }

    @PatchMapping(value = "/cars/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<CarDTO> partialUpdateCar(
        Principal principal,
        @PathVariable(value = "id", required = false) final String id,
        @NotNull @RequestBody CarDTO carDTO
    ) {
        log.debug("REST request to partial update Car partially : {}, {}", id, carDTO);
        validateForUpdate(principal, id, carDTO);

        Optional<CarDTO> result = carService.partialUpdate(carDTO);

        return ResponseUtil.wrapOrNotFound(result, HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, carDTO.getId()));
    }

    @GetMapping("/cars")
    public List<CarDTO> getAllCars() {
        log.debug("REST request to get all Cars");
        return carService.findAll();
    }

    @GetMapping("/my-cars")
    public List<CarDTO> getMyCars(Principal principal) {
        return carService.findAllByUsername(principal.getName());
    }

    @GetMapping("/cars/{id}")
    public ResponseEntity<CarDTO> getCar(@PathVariable String id) {
        log.debug("REST request to get Car : {}", id);
        Optional<CarDTO> carDTO = carService.findOne(id);
        return ResponseUtil.wrapOrNotFound(carDTO);
    }

    @DeleteMapping("/cars/{id}")
    public ResponseEntity<Void> deleteCar(Principal principal, @PathVariable String id) {
        log.debug("REST request to delete Car : {}", id);

        validateExistenceAndOwnership(principal, id);

        carService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }

    private void validateForUpdate(Principal principal, @PathVariable(value = "id", required = false) String id, @RequestBody @NotNull CarDTO carDTO) {
        if (carDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, carDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }
        validateExistenceAndOwnership(principal, carDTO.getId());

        carDTO.setUsername(principal.getName());
    }

    private void validateExistenceAndOwnership(Principal principal, String id) {
        var carOpt = carService.findOne(id);
        if (carOpt.isEmpty()) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        var car = carOpt.get();
        if (!car.getUsername().equals(principal.getName())) {
            throw new BadRequestAlertException("Forbidden " + car.getUsername() + " " + principal.getName(), ENTITY_NAME, "forbidden");
        }
    }
}
