/**
 * View Models used by Spring MVC REST controllers.
 */
package com.uberclone.profile.web.rest.vm;
